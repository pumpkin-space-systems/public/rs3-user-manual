------------------
External Resources
------------------

This is the external resources page that contains links to various documentation for software and hardware pertaining to
the Pumpkin FlatSat and RS3 platforms. This includes

* Hardware resources

    * Datasheets
    * User Manuals

* Software resources

    * Pumpkin-provided interface libraries
    * PuTDIG software
    * KubOS resources

Hardware Resources
==================

The following sections is a listing of hardware avionics customer resources provided for an RS3/FlatSat unit

Datasheets
**********

Public datasheets `can be found here <http://www.pumpkininc.com/space/datasheet/>`_.

.. note:: Datasheets for certain SUPERNOVA modules can be acquired upon request.

User Manuals
************

Public manuals `are be found here <http://www.pumpkininc.com/space/manual/>`_.

.. note:: Manuals for certain SUPERNOVA modules can be acquired upon request.

Software Resources
==================

The following sections is a listing of the software resources available for the customer in a RS3/FlatSat unit.

Pumpkin avionics libraries/software
***********************************

The table contains platform agnostic Pumpkin software/libraries that could be integrated into a given Flight software solution

.. csv-table:: Pumpkin-provided software
    :header: "Software", "Link/Path in Delivery"
    :widths: 20, 80

    "pumpkin_supmcu", ":ref:`pumpkin-supmcu`"
    "Pumqry", ":ref:`pumqry`"
    "Pumgen", ":ref:`pumgen`"
    "Puminj", ":ref:`puminj`"

KubOS Flight Software Resources
*******************************

The following contains links to the relevant KubOS Flight software sections.

.. csv-table:: KubOS Links
    :header: "Software", "Link/Path in Delivery"
    :widths: 20, 80

    "KubOS Main Documentation", "`<https://docs.kubos.com/1.21.0/>`_"
    "GraphQL", "`<https://graphql.org/learn>`_"
    "Mission Apps Example", "`<https://gitlab.com/pumpkin-space-systems/public/mission-apps>`_"
    "Pumpkin KubOS Fork", "`<https://gitlab.com/pumpkin-space-systems/public/kubos>`_"
    "Apollo Fusion Mission examples", "`<https://github.com/kubos/apollo-fusion>`_"

.. note:: Any flight-software could be written for Pumpkin avionics, currently a fork of KubOS is under active development by Pumpkin software engineers.
