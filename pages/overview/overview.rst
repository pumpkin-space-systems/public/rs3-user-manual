----------------------
Avionics Overview
----------------------

The following sections cover:

* Rackmount Simulator (RS3)

    * Overview of unit
    * Features/Options
    * Intended Usage

* FlatSat (Engineering Model Avionics Bus)

    * Overview of unit
    * Features
    * Differences from Flight-Model (FM) Avionics Bus
    * Intended Usage

* Differences between RS3 & FlatSat

    * Developmental differences
    * Electrical differences
    * When to use RS3 vs FlatSat

Rackmount Simulator
===================

The Rackmount Simulator (RS3) is a 2U, rack-mounted simulated FlatSat unit pre-installed with a SUPERNOVA C&DH (MBM2),
two QSM modules and a number of optional features.

.. RS3 PICTURE

Overview/Features
*****************

The RS3 unit is a stand-in for simulating the telemetry and command output of any SUPERNOVA Avionics Bus module via a set of Quad-Simulator Modules (QSM).
Simulatable modules on the QSM include:

* ADCS Interface Module, 2 (AIM2)
* Battery Module, 2 (BM2)
* Battery Switch Module (BSM)
* Bus Interface Module (BIM)
* Electrical Power System Module (EPSM)
* GNSS Receiver Module (GNSSRM)
* Payload Interface Module (PIM)
* Radio Host Module (RHM)
* Solar Interface Module (SIM)
* Any module with a Supervisor MCU ...

The simulated capabilities **do NOT** include the electrical interfaces, just command and telemetry input/outputs.
The module includes the capability to set the current telemetry values returned by the module for testing specific code paths inside of the user's flight software;
see :ref:`putdig-sw`.
In addition, some modules can emulate runtime behaviors (such as the EPSM's automatic restart if the Watchdog Timer is not kicked).

.. QSM PICTURE WITH CAPTION

The RS3 includes an Motherboard Module 2 (MBM2) pre-installed as the C&DH of the system to allow development of the bus Flight Software.
Additional purchase options can include pre-installing:

* MAI-401 ADCS Simulator Board
* MBM2 RevF for RS-422 connectivity
* GNSSRM Module

The RS3 includes a CubeSat Kit Bus inside of the unit that could be used to install additional CSK-compatable modules.
Please contact Pumpkin for more information on electrical compatibilities of first/third-party modules.

.. attention:: Please exercise caution when connecting new modules to the CSK bus, contact Pumpkin if unsure or have questions about electrical connections.

For more information on the electrical interfaces available, see :ref:`rs3-electrical-interfaces`.

.. RS3 PICTURE WITH OPTIONS?

Intended Usages
***************

The RS3 is intended to be used as a first-article towards developing Mission and Flight Software.
For example, the RS3 can be used to simulate reacting to certain avionics state conditions by injecting telemetry into the QSM modules installed into the RS3.
Other possible usages:

* Initial design and implementation of new Flight Software for SUPERNOVA Avionics
* Integration of mission applications with existing :ref:`KubOS <kubos-usage-architecture>` for mission
* Initial prototyping and integration with payload hardware (via RS232/422/Ethernet) links
* Continuous Integration and testing setup for Flight Software with simulatable modules.

The RS3 is a versatile platform that allows the customer to develop most/all of the software before interfacing with an FlatSat or Flight-model avionics bus.

FlatSat
=======

The FlatSat is an Engineering model avionics bus that has none to few electrical interface differences from a flight-model avionics bus, allowing for close
emulation of software written for SUPERNOVA avionics.

.. FLATSAT PICTURE

Overview/Features
*****************

The FlatSat is suitable for testing both Software and Electrical interfaces of the avionics stack; the FlatSat uses the same SUPERNOVA modules
as the Flight-Model in a ground-testing configuration (non-staked). All of the possible avionics modules can be installed into a FlatSat, with the proper configuration.
This includes:

* Supervisor MCU Modules (PIM/BIM/SIM...)
* ADCS Module (through simulator modules)
* Electrical Power System (EPSM)
* Avionics Radios (Simplex/Duplex/XLINK/SRS3...)
* GNSS Testing (GNSSRM)

FlatSat vs Flight Model
^^^^^^^^^^^^^^^^^^^^^^^

The key differences between the FlatSat SUPERNOVA avionics and Flight-model avionics is:

* The Flight-model avionics are all staked and assembled for vibration testing

    * The FlatSat is able to be easily taken apart and reconfigured, where-as flight hardware may be staked and assembled, requiring lengthier and riskier disassembly if needed.
    * Some connectors are soldered instead of connectorized in the flight configuration as well.

* The BeagleBone Black on the MBM2 will have a custom Ethernet daughter-board with DF-13-8 pin connector for Ethernet connectivity

    * The FlatSat has the standard RJ-45 connector on the BBB

* The FlatSat does not typically include the necessary stack adapters and mechanical hardware to mount inside of a Spacecraft chassis.

    * The Flight avionics require additional manufacturing steps to enable mounting within a Spacecraft chassis (e.g. soldering the PIM to the RHM)

.. PICTURE OF FM?

FlatSat vs RS3
^^^^^^^^^^^^^^

The FlatSat contains more electrical functionality and is in a friendlier form-factor for bench-top testing.
Key differences include:

* The FlatSat is in a stacked configuration vs a development motherboard layout.

    * The stacked configuration uses less desk-space, making it more suitable for bench-top testing

* The FlatSat includes the avionics modules needed to make the electrical connections to the Payload

    * The RS3 includes a limited set of electrical connections compared to the FlatSat unit, where-as the FlatSat contains the same amount of electrical connections as the Flight model.

The FlatSat is a natural evolution from the RS3; the FlatSat is meant to facilitate the end-to-end testing once mission software functionality is verified in the RS3.

Intended Usage
**************

The FlatSat is intended to be used for end-to-end testing with the Payload and Bus avionics stack for:

* Verification of Flight Software functionality on SUPERNOVA avionics

    * The RS3 would be used as an initial development tool, if needed, with the FlatSat being used before testing with the Flight Avionics
    * Orbital scenarios for the mission can be emulated directly on the FlatSat (e.g. power-negative condition)

* Testing electrical interfaces

    * End-to-end testing with payload connections for connectivity and power
    * Qualification of integrations with the avionics bus (e.g. power draw of payload when connected to EPSM)

* Initial integration testing of payload and flight bus

    * Testing of communication interfaces to/from the C&DH to the Payload
    * Powering the payload from PIM

The FlatSat can be directly used for initial development of Flight software as well.
