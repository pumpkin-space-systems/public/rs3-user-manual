----------
KubOS SDK
----------

.. attention:: The KubOS SDK is deprecated and not recommended for production usage. This section will be removed/replaced in a future update.

    It is recommended to setup the environment for KubOS in an Ubuntu VM or similar, and follow :ref:`setup-process`

The following is a quick-start meant to detail:

* Initial configuration of Vagrant for faster build-times/ease of data sharing from<->to VM
* Links/directions for Pumpkin maintained forks of KubOS operating system and middle-ware
* Configuration of build-root to include Pumpkin PuTDIG and other RS3/FlatSat software
* Recommended edits to filesystem/configuration of KubOS image for easier usage in lab

For full information about the KubOS SDK, please visit the `documentation here <https://docs.kubos.com/1.21.0/sdk-docs/index.html>`_.

KubOS SDK Vagrant Setup
=======================

The installation of the KubOS SDK is meant to be a simple and straightforward process.
The section below details briefly how to setup the SDK as well as some recommended configuration/folder layouts.

.. note:: For full installation instructions, please visit `the KubOS documentation here <https://docs.kubos.com/1.21.0/sdk-docs/sdk-installing.html#installing-the-kubos-sdk>`_

Dependency Setup
****************

The KubOS SDK requires the following dependencies:

* `Vagrant <https://www.vagrantup.com/docs/installation>`_
* `VirtualBox <https://www.virtualbox.org/wiki/Downloads>`_
* `VirtualBox Extension Pack <https://download.virtualbox.org/virtualbox/6.1.12/Oracle_VM_VirtualBox_Extension_Pack-6.1.12.vbox-extpack>`_

Install all of the dependencies listed above

After installation, if on Linux based operating systems, run the following from the terminal ::

    $ sudo usermod -aG vboxusers <username>

This allows pass-through of USB devices to the KubOS SDK such as the USB Debug Adapter.

.. _create-sdk-instance:

Create SDK Instance
*******************

In order to create the instance, open a terminal in the location to house the instance and enter the following ::

    $ vagrant init kubos/kubos-dev
    $ vagrant up

The ``vagrant init`` command creates a ``Vagrantfile`` in the current working directory and the ``vagrant up`` command downloads and installs the KubOS SDK instance.
Note the ``vagrant up`` command will take a while to finish if run for the first time.

Once the commands finish executing successfully, its recommended to edit the ``Vagrant`` file in the current working directory to change:

* The amount of CPU cores allocated to the VM
* The amount of memory allocated to the VM
* Allow access to the network from the VM
* Shared folders from host computer to VM

To adjust the CPU cores and memory, find the section ``config.vm.provider ...`` in the ``Vagrant`` file and change it to contain ::

    config.vm.provider "virtualbox" do |vb|
        # Customize the amount of memory/cores on the VM:
        vb.memory = "4096" # Amount of memory in MB
        vb.cpus = "4"      # Number of CPU cores
    end

To add a shared folder to the VM, find the line ``# config.vm.synced_folder ...`` and change to ::

    config.vm.synced_folder "./data", "/mnt/data" # "path/on/host", "/path/inside/VM"

To enable the Vagrant VM to access the network connected to the host machine, find the line ``# config.vm.network "private_network" ...`` and change to ::

    config.vm.network "private_network", ip: "192.168.33.10"

Once these edits are done, the Vagrant VM can be reloaded via ``vagrant reload`` command in the terminal

Use the ``vagrant ssh`` to access the VM machine.

.. attention:: When the KubOS SDK is running via ``vagrant up``, the VM will **automatically** take ownership of USB Serial devices such as the USB Debug Adapter.
