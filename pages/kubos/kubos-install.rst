----------------
Installing KubOS
----------------

The following section covers the install of KubOS on RS3 and FlatSat units.
Note the software installation process of KubOS is the same between the RS3 <-> FlatSat, however, isolating the MBM2 from the RS3 is recommended due to limited space around the ``S2`` button and Debug port.

.. attention::
    All equipment is ESD-sensitive, please wear a grounding strap or equivalent and work on an anti-static surface while servicing hardware.

+++++++++++++++++++++++
RS3-Specific Procedures
+++++++++++++++++++++++

Installing KubOS into the RS3's MBM2 is pretty straightforward, with two recommended methods of installing KubOS:

* BBB integrated onto the MBM2 from an SSH session over Ethernet
* BBB de-integrated (removed) from MBM2

Both methods have the user open the RS3 and remove the MBM2 module from the RS3 unit since the ``S2 (MicroSD Boot)`` button must be pressed while powering on.

.. attention::
    The RS3 comes **pre-installed** with KubOS.

    This process is **only** necessary if:

    * The base image is corrupt and recovery is not possible via `KubOS Recovery Process <https://docs.kubos.com/1.21.0/ecosystem/linux-docs/kubos-linux-recovery.html#kubos-linux-recovery-architecture>`_
    * Major component/service upgrades are made (e.g. kernel/major system package upgrades). See note in `KubOS Upgrade Overview <https://docs.kubos.com/1.21.0/ecosystem/linux-docs/kubos-linux-upgrade.html#overview>`_
    * New BBB/MBM2 is placed into RS3 unit

    All other OS updates should be done through the :doc:`KubOS Upgrade Process </pages/kubos/kubos-upgrade>`

.. note:: For full instructions, please see `the KubOS Documentation for Installation <https://docs.kubos.com/1.21.0/obc-docs/mbm2/installing-linux-mbm2.html#installing-kubos-linux-on-a-pumpkin-motherboard-module-2>`_

Isolating MBM2+BBB for RS3
==========================

Installing KubOS from the MicroSD Install image involves removing the MBM2 from the RS3 unit.

Tools needed are:

* PH1 Screwdriver
* PH0 Screwdriver
* USB Mini Type-B to Type-A
* 2.0mm Hex Screwdriver (If removing BBB from MBM2)
* Wooden stick/non-conductive tool (If leaving BBB on MBM2)

.. danger:: There are **lethal** voltages present inside of the RS3 **while powered**. Please turn off the unit and unplug before proceeding.

.. attention::
    Please exercise caution and wear ESD protection when interacting with the internals of the RS3 unit.

Removal of Top Cover
********************

To remove the top cover, there are 4 #PH1 Screws holding the top cover on the RS3.
Remove the two on each side of the unit (right side shown below):

.. image:: /img/kubos-install-1.jpg

Place the screws aside, and remove the top cover vertically.
This should expose the RS3 motherboard and internals.

.. image:: /img/rs3-internals.JPG

Place the KubOS Install image into the MicroSD card slot at this time:

.. image:: /img/kubos-install-2.jpg

Removal of MBM2
***************

Next the MBM2 must be removed from the RS3 to allow the user to safely press the ``S2`` MicroSD card boot button.

Remove the 4x PH0 screws from the MBM2:

.. image:: /img/kubos-install-3.jpg

Gently wiggle the MBM2 module from **left-to-right** while pulling up gently on the module to free from the CSK header

.. attention:: Exercise caution while removing the module from the CSK header. The H1 & H2 pins are **easily** bent if too much force is used.

Remove the Ethernet, Mini-USB and USB-A cables connected to the BBB and place MBM2 on non-conductive ESD-safe surface.

Next follow either :ref:`booting-installed-into-mbm2` or :ref:`booting-install-bbb-alone`.

++++++++++++++++++++++++++++++++
FlatSat Specific Instructions
++++++++++++++++++++++++++++++++

Installing KubOS on a FlatSat is very easy; no de-integration of the MBM2 from the FlatSat is required, since the ``S2`` button is accessible.
There is only two steps required:

* Suppress the Bus WDT (if present). The BIM/AIM2/RHM1 have WDT circuitry. There will be a two-pin right-angle header where a shorting-block can be installed to suppress the WDT from resetting the bus.
    + Consult specific datasheet(s) for more information on suppression. The shorting-block comes installed with a new FlatSat.
* Remove Auxiliary image MicroSD card, if present, then insert the installation image into the MicroSD card slot on the BBB.

Follow steps for :ref:`booting-connected-debug-adapter`.

+++++++++++++++++++++++++++
Booting into KubOS Install
+++++++++++++++++++++++++++

There are three methods available to boot then connect off the KubOS install image (instead of BBB internal eMMC):

* Connect USB Debug Adapter and hold any key while BBB is powering up
* Boot while holding ``S2`` then connecting via Ethernet
* Boot while holding ``S2`` then connecting via FTDI TTL-RS232 3.3V with the BBB on its own

.. _booting-connected-debug-adapter:

Booting install via USB Debug Adapter
=====================================

This method is recommended on the FlatSat since this only requires a connection to the 4-Pin FPC port on the MBM2.

First, connect the 4-pin FPC cable to the MBM2 Debug Port ``J132``.

Second, connect the USB Debug Adapter to the computer and open up PuTTy (or equivalent Serial console such as ``screen`` or ``minicom`` on Linux).
Use the following settings for serial access:

* Baudrate: ``115200``
* Bits: ``8``
* Parity: ``N``
* Stop-bits: ``1``
* Handshake: ``None``

Open the serial console and then connect power to the DCPS via 24V Mini-XLR Power Adapter provided with the FlatSat delivery.
After connecting power, immediately hold down any key while in the serial console to get the ``U-Boot>`` prompt.
If the ``U-Boot>`` prompt is not shown, please try again from removing power then holding enter after applying power.

Once the ``U-Boot`` prompt is up, enter the following command into the console ::

    setenv bootargs console=ttyS0,115200 root=/dev/mmcblk0p2 ext4 rootwait; fatload mmc 0:1 ${fdtaddr} /pumpkin-mbm2.dtb; fatload mmc 0:1 ${loadaddr} /kernel; bootm ${loadaddr} - ${fdtaddr}

The following should be the output in the serial console for the boot of the KubOS Install image ::

    U-Boot SPL 2016.09 (Jul 31 2020 - 13:30:52)
    Trying to boot from MMC1
    MMC Device 1 not found
    ** Bad device mmc 1 **
    Using default environment

    reading u-boot.img
    reading u-boot.img


    U-Boot 2016.09 (Jul 31 2020 - 13:30:52 -0700)

    I2C:   ready
    DRAM:  512 MiB
    MMC:   OMAP SD/MMC: 0, OMAP SD/MMC: 1
    ** File not found /uboot.env **

    ** Unable to read "/uboot.env" from mmc1:3 **
    Using default environment

    Net:   <ethaddr> not set. Validating first E-fuse MAC
    cpsw, usb_ether
    Hit any key to stop autoboot:  0
    Successfully updated envars
    ## Error: "bootcmd_legacy_mmc${boot_dev}" not defined
    switch to partitions #0, OK
    mmc0 is current device
    SD/MMC found on device 0
    reading /kernel
    2586772 bytes read in 166 ms (14.9 MiB/s)
    reading /pumpkin-mbm2.dtb
    32536 bytes read in 9 ms (3.4 MiB/s)
    ## Loading kernel from FIT Image at 82000000 ...
       Using 'config@1' configuration
       Verifying Hash Integrity ... OK
       Trying 'kernel@1' kernel subimage
         Description:  unavailable
         Created:      2020-08-14  20:09:51 UTC
         Type:         Kernel Image
         Compression:  uncompressed
         Data Start:   0x820000a8
         Data Size:    2585224 Bytes = 2.5 MiB
         Architecture: ARM
         OS:           Linux
         Load Address: 0x82800000
         Entry Point:  0x82800000
         Hash algo:    sha256
         Hash value:   1b3051c1a9890745cba16fe9fcf906c7fed9b7880fb2807fc60bf0ee211f86ee
       Verifying Hash Integrity ... sha256+ OK
    ## Flattened Device Tree blob at 88000000
       Booting using the fdt blob at 0x88000000
       Loading Kernel Image ... OK
       Loading Device Tree to 8fff5000, end 8fffff17 ... OK

    Starting kernel ...

    Booting Linux on physical CPU 0x0
    Linux version 4.4.23-KubOS-0.0.0 (root@ubuntu1804.localdomain) (gcc version 7.4.0 (Buildroot 2019.02.2) ) #1 Fri Jul 31 13:36:36 PDT 2020
    CPU: ARMv7 Processor [413fc082] revision 2 (ARMv7), cr=10c5387d
    CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
    Machine model: Pumpkin MBM2
    Memory policy: Data cache writeback
    CPU: All CPU(s) started in SVC mode.
    AM335X ES2.1 (sgx neon )
    Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 130048
    Kernel command line: console=ttyS0,115200 root=PARTUUID=4b4c4e58-02 ro rootfstype=ext4 rootwait
    PID hash table entries: 2048 (order: 1, 8192 bytes)
    Dentry cache hash table entries: 65536 (order: 6, 262144 bytes)
    Inode-cache hash table entries: 32768 (order: 5, 131072 bytes)
    Memory: 515160K/524288K available (2880K kernel code, 111K rwdata, 1048K rodata, 172K init, 197K bss, 9128K reserved, 0K cma-reserved, 0K highmem)
    Virtual kernel memory layout:
        vector  : 0xffff0000 - 0xffff1000   (   4 kB)
        fixmap  : 0xffc00000 - 0xfff00000   (3072 kB)
        vmalloc : 0xe0800000 - 0xff800000   ( 496 MB)
        lowmem  : 0xc0000000 - 0xe0000000   ( 512 MB)
        pkmap   : 0xbfe00000 - 0xc0000000   (   2 MB)
        modules : 0xbf000000 - 0xbfe00000   (  14 MB)
          .text : 0xc0008000 - 0xc03de384   (3929 kB)
          .init : 0xc03df000 - 0xc040a000   ( 172 kB)
          .data : 0xc040a000 - 0xc0425ec0   ( 112 kB)
           .bss : 0xc0425ec0 - 0xc04572f0   ( 198 kB)
    NR_IRQS:16 nr_irqs:16 16
    IRQ: Found an INTC at 0xfa200000 (revision 5.0) with 128 interrupts
    OMAP clockevent source: timer2 at 24000000 Hz
    sched_clock: 32 bits at 24MHz, resolution 41ns, wraps every 89478484971ns
    clocksource: timer1: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 79635851949 ns
    OMAP clocksource: timer1 at 24000000 Hz
    Calibrating delay loop... 996.14 BogoMIPS (lpj=4980736)
    pid_max: default: 32768 minimum: 301
    Mount-cache hash table entries: 1024 (order: 0, 4096 bytes)
    Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes)
    CPU: Testing write buffer coherency: ok
    Setting up static identity map for 0x80008200 - 0x80008258
    devtmpfs: initialized
    VFP support v0.3: implementor 41 architecture 3 part 30 variant c rev 3
    omap_hwmod: tptc0 using broken dt data from edma
    omap_hwmod: tptc1 using broken dt data from edma
    omap_hwmod: tptc2 using broken dt data from edma
    omap_hwmod: debugss: _wait_target_disable failed
    clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
    pinctrl core: initialized pinctrl subsystem
    NET: Registered protocol family 16
    DMA: preallocated 256 KiB pool for atomic coherent allocations
    cpuidle: using governor ladder
    cpuidle: using governor menu
    OMAP GPIO hardware version 0.1
    edma 49000000.edma: Legacy memcpy is enabled, things might not work
    edma 49000000.edma: TI EDMA DMA engine driver
    usbcore: registered new interface driver usbfs
    usbcore: registered new interface driver hub
    usbcore: registered new device driver usb
    omap_i2c 44e0b000.i2c: could not find pctldev for node /ocp/l4_wkup@44c00000/scm@210000/pinmux@800/pinmux_i2c0_pins, deferring probe
    omap_i2c 4802a000.i2c: could not find pctldev for node /ocp/l4_wkup@44c00000/scm@210000/pinmux@800/pinmux_i2c1_pins, deferring probe
    EDAC MC: Ver: 3.0.0
    clocksource: Switched to clocksource timer1
    NET: Registered protocol family 2
    TCP established hash table entries: 4096 (order: 2, 16384 bytes)
    TCP bind hash table entries: 4096 (order: 2, 16384 bytes)
    TCP: Hash tables configured (established 4096 bind 4096)
    UDP hash table entries: 256 (order: 0, 4096 bytes)
    UDP-Lite hash table entries: 256 (order: 0, 4096 bytes)
    NET: Registered protocol family 1
    futex hash table entries: 256 (order: -1, 3072 bytes)
    VFS: Disk quotas dquot_6.6.0
    VFS: Dquot-cache hash table entries: 1024 (order 0, 4096 bytes)
    Block layer SCSI generic (bsg) driver version 0.4 loaded (major 251)
    io scheduler noop registered
    io scheduler deadline registered
    io scheduler cfq registered (default)
    pinctrl-single 44e10800.pinmux: 142 pins at pa f9e10800 size 568
    Serial: 8250/16550 driver, 6 ports, IRQ sharing disabled
    console [ttyS0] disabled
    44e09000.serial: ttyS0 at MMIO 0x44e09000 (irq = 155, base_baud = 3000000) is a 8250
    console [ttyS0] enabled
    48022000.serial: ttyS1 at MMIO 0x48022000 (irq = 156, base_baud = 3000000) is a 8250
    48024000.serial: ttyS2 at MMIO 0x48024000 (irq = 157, base_baud = 3000000) is a 8250
    481a6000.serial: ttyS3 at MMIO 0x481a6000 (irq = 158, base_baud = 3000000) is a 8250
    481a8000.serial: ttyS4 at MMIO 0x481a8000 (irq = 159, base_baud = 3000000) is a 8250
    481aa000.serial: ttyS5 at MMIO 0x481aa000 (irq = 160, base_baud = 3000000) is a 8250
    davinci_mdio 4a101000.mdio: davinci mdio revision 1.6
    davinci_mdio 4a101000.mdio: detected phy mask fffffffe
    libphy: 4a101000.mdio: probed
    davinci_mdio 4a101000.mdio: phy[0]: device 4a101000.mdio:00, driver SMSC LAN8710/LAN8720
    cpsw 4a100000.ethernet: Detected MACID = 80:30:dc:90:d5:4f
    SLIP: version 0.8.4-NET3.019-NEWTTY (dynamic channels, max=256) (6 bit encapsulation enabled).
    CSLIP: code copyright 1989 Regents of the University of California.
    SLIP linefill/keepalive option.
    ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
    ehci-omap: OMAP-EHCI Host Controller driver
    i2c /dev entries driver
    omap_wdt: OMAP Watchdog Timer Rev 0x01: initial timeout 60 sec
    softdog: Software Watchdog Timer: 0.08 initialized. soft_noboot=0 soft_margin=60 sec soft_panic=0 (nowayout=0)
    omap_hsmmc 48060000.mmc: Got CD GPIO
    mmc0: mmc_rescan_try_freq: trying to init card at 400000 Hz
    mmc0: host does not support reading read-only switch, assuming write-enable
    ledtrig-cpu: registered to indicate activity on CPUs
    ti_am3359-tscadc 44e0d000.tscadc: chan 0 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 1 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 2 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 3 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 4 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 5 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 6 step_avg truncating to 16
    mmc0: new high speed SDHC card at address 5048
    mmcblk0: mmc0:5048 SD16G 14.5 GiB
    NET: Registered protocol family 17
    sctp: Hash tables configured (established 512 bind 1024)
    omap_voltage_late_init: Voltage driver support not added
    ThumbEE CPU extension supported.
     mmcblk0: p1 p2 p3 p4
    mmc1: mmc_rescan_try_freq: trying to init card at 400000 Hz
    tps65217 0-0024: TPS65217 ID 0xe version 1.2
    omap_i2c 44e0b000.i2c: bus 0 rev0.11 at 400 kHz
    rtc-m41t80 1-0068: read error
    mmc1: MAN_BKOPS_EN bit is not set
    rtc-m41t80 1-0068: rtc core: registered m41t81s as rtc0
    rtc-m41t80 1-0068: Can't clear HT bit
    rtc-m41t80: probe of 1-0068 failed with error -121
    omap_i2c 4802a000.i2c: bus 1 rev0.11 at 100 kHz
    hctosys: unable to open rtc device (rtc0)
    EXT4-fs (mmcblk0p2): INFO: recovery required on readonly filesystem
    EXT4-fs (mmcblk0p2): write access will be enabled during recovery
    mmc1: new high speed MMC card at address 0001
    mmcblk1: mmc1:0001 S10004 3.56 GiB
    mmcblk1boot0: mmc1:0001 S10004 partition 1 4.00 MiB
    mmcblk1boot1: mmc1:0001 S10004 partition 2 4.00 MiB
     mmcblk1: p1 p2 p3 p4
    EXT4-fs (mmcblk0p2): recovery complete
    EXT4-fs (mmcblk0p2): mounted filesystem with ordered data mode. Opts: (null)
    VFS: Mounted root (ext4 filesystem) readonly on device 179:2.
    devtmpfs: mounted
    Freeing unused kernel memory: 172K (c03df000 - c040a000)
    fsck 1.44.5 (15-Dec-2018)
    CP437: Invalid argument
    fsck.fat 4.1 (2017-01-24)
    /dev/mmcblk0p1: 7 files, 1489/8167 clusters
    fsck 1.44.5 (15-Dec-2018)
    rootfs: clean, 3508/20480 files, 76937/81920 blocks
    fsck 1.44.5 (15-Dec-2018)
    rootfs: recovering journal
    rootfs: clean, 3508/20480 files, 76935/81920 blocks
    fsck 1.44.5 (15-Dec-2018)
    /dev/mmcblk1p3 contains a file system with errors, check forced.
    /dev/mmcblk1p3: Entry 'uboot.env' in / (2) has filetype set.
    CLEARED.
    /dev/mmcblk1p3: 12/128 files (0.0% non-contiguous), 1073/2048 blocks
    fsck 1.44.5 (15-Dec-2018)
    /dev/mmcblk1p4: clean, 30/196608 files, 46769/3145728 blocks
    fsck 1.44.5 (15-Dec-2018)
    /dev/mmcblk0p3: recovering journal
    /dev/mmcblk0p3: clean, 11/128 files, 1063/2048 blocks
    fsck 1.44.5 (15-Dec-2018)
    /dev/mmcblk0p4: clean, 30/196608 files, 46769/3145728 blocks
    random: nonblocking pool is initialized
    EXT4-fs (mmcblk0p2): re-mounted. Opts: (null)
    EXT4-fs (mmcblk0p4): mounted filesystem with ordered data mode. Opts: (null)
    mount: /home/microsd/: can't find PARTUUID=41555820-02.
    mount: /upgrade: can't find PARTUUID=41555820-01.
    EXT4-fs (mmcblk0p3): mounted filesystem with ordered data mode. Opts: (null)
    Starting rsyslogd: OK
    Initializing random number generator... done.
    Starting network: net eth0: initializing cpsw version 1.12 (0)
    net eth0: phy found : id is : 0x7c0f1
    libphy: PHY 4a101000.mdio:01 not found
    net eth0: phy 4a101000.mdio:01 not found on slave 1
    OK
    Starting dropbear sshd: OK
    Starting kubos-app-service:
    OK
    Starting file-service:
    OK
    Starting shell-service:
    OK
    Starting telemetry-service:
    OK
    Starting mai400-service:
    OK
    Starting monitor-service:
    OK
    Starting novatel-oem6-service:
    OK
    Starting scheduler-service:
    OK
    Cannot open /envar/uboot.env: No such file or directory
    Error: environment not initialized
    Cannot open /envar/uboot.env: No such file or directory
    Error: environment not initialized
    Starting Monit 5.25.2 daemon with http interface at [*]:7000
    'Kubos' Monit 5.25.2 started

    Welcome to Kubos Linux
    Kubos login: 'kubos-mai400' process is not running
    'kubos-mai400' trying to restart
    'kubos-mai400' start: '/etc/init.d/S90kubos-mai400 start'
    Traceback (most recent call last):
      File "/usr/sbin/pumpkin-mcu-service/service.py", line 36, in <module>
        schema.MODULES = c.raw['modules']
    KeyError: 'modules'

    Welcome to Kubos Linux
    Kubos login:

For more information on this method, see `KubOS Documentation <https://docs.kubos.com/1.21.0/obc-docs/mbm2/installing-linux-mbm2.html#boot-into-u-boot>`_.
Once booted, the KubOS Image can be installed now, see :ref:`install-kubos-from-image`

.. _booting-installed-into-mbm2:

Booting install while Integrated with MBM2 (via Ethernet)
=========================================================

This method is recommended on the RS3 since the BBB does not have to be removed from the MBM2, risking possible damage to the pins on the MBM2.

.. note::
    This process requires that the user has configured the KubOS image to be able to connect to the local network.
    Please follow the directions in the :doc:`KubOS SDK Documentation </pages/kubos/kubos-sdk>` recommended configurations for more information.

Connect an Ethernet cable to the BBB's RJ-45 jack.
After connecting, press and hold the ``S2`` (MicroSD card boot) button (shown below) with a wooden/non-conductive stick/tool.

.. image:: /img/kubos-install-4a.jpg

**While holding the S2 Button** plug in the Mini USB cable to power the MBM2+BBB. After a few seconds, the ``S2`` button can be depressed.

SSH into the KubOS board at the IP address configured when creating the OS image. Follow directions for KubOS Install.

.. _booting-install-bbb-alone:

Booting install with BBB alone
==============================

This method is the only method available if:

* The image does not have an IP address configured OR the user does not have access to a local network for the BBB.
* The user does not have a USB Debug Adapter.

.. note:: An `FTDI TTL-RS232 3.3V <https://www.amazon.com/Converter-Terminated-Galileo-BeagleBone-Minnowboard/dp/B06ZYPLFNB/>`_ cable is required.

.. attention:: Exercise caution while removing BBB from MBM2. The pins on the MBM2 **are easily bent** if too much force is applied.

Remove the 4x 2mm Hex screws shown below:

.. image:: /img/kubos-install-4b.jpg

Remove the BBB from the MBM2 by gently rocking it from **left-to-right** while pulling up on the BBB.

Connect the FTDI cable to the ``J1`` header on the BBB, with the GND closest to the white dot on the silk-screen. Open a serial console at ``115200`` bps baud to the COM (or device path) represented by FTDI cable.

Once removed, press and hold the ``S2`` (MicroSD card boot) button (shown below):

.. image:: /img/kubos-install-5.jpg

**While holding the S2 button** plug in the Mini USB cable to power the BBB. After a few seconds, the ``S2`` button can be depressed.

The following should be the output in the serial console for the boot of the KubOS Install image ::

    U-Boot SPL 2016.09 (Jul 31 2020 - 13:30:52)
    Trying to boot from MMC1
    MMC Device 1 not found
    ** Bad device mmc 1 **
    Using default environment

    reading u-boot.img
    reading u-boot.img


    U-Boot 2016.09 (Jul 31 2020 - 13:30:52 -0700)

    I2C:   ready
    DRAM:  512 MiB
    MMC:   OMAP SD/MMC: 0, OMAP SD/MMC: 1
    ** File not found /uboot.env **

    ** Unable to read "/uboot.env" from mmc1:3 **
    Using default environment

    Net:   <ethaddr> not set. Validating first E-fuse MAC
    cpsw, usb_ether
    Hit any key to stop autoboot:  0
    Successfully updated envars
    ## Error: "bootcmd_legacy_mmc${boot_dev}" not defined
    switch to partitions #0, OK
    mmc0 is current device
    SD/MMC found on device 0
    reading /kernel
    2586772 bytes read in 166 ms (14.9 MiB/s)
    reading /pumpkin-mbm2.dtb
    32536 bytes read in 9 ms (3.4 MiB/s)
    ## Loading kernel from FIT Image at 82000000 ...
       Using 'config@1' configuration
       Verifying Hash Integrity ... OK
       Trying 'kernel@1' kernel subimage
         Description:  unavailable
         Created:      2020-08-14  20:09:51 UTC
         Type:         Kernel Image
         Compression:  uncompressed
         Data Start:   0x820000a8
         Data Size:    2585224 Bytes = 2.5 MiB
         Architecture: ARM
         OS:           Linux
         Load Address: 0x82800000
         Entry Point:  0x82800000
         Hash algo:    sha256
         Hash value:   1b3051c1a9890745cba16fe9fcf906c7fed9b7880fb2807fc60bf0ee211f86ee
       Verifying Hash Integrity ... sha256+ OK
    ## Flattened Device Tree blob at 88000000
       Booting using the fdt blob at 0x88000000
       Loading Kernel Image ... OK
       Loading Device Tree to 8fff5000, end 8fffff17 ... OK

    Starting kernel ...

    Booting Linux on physical CPU 0x0
    Linux version 4.4.23-KubOS-0.0.0 (root@ubuntu1804.localdomain) (gcc version 7.4.0 (Buildroot 2019.02.2) ) #1 Fri Jul 31 13:36:36 PDT 2020
    CPU: ARMv7 Processor [413fc082] revision 2 (ARMv7), cr=10c5387d
    CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
    Machine model: Pumpkin MBM2
    Memory policy: Data cache writeback
    CPU: All CPU(s) started in SVC mode.
    AM335X ES2.1 (sgx neon )
    Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 130048
    Kernel command line: console=ttyS0,115200 root=PARTUUID=4b4c4e58-02 ro rootfstype=ext4 rootwait
    PID hash table entries: 2048 (order: 1, 8192 bytes)
    Dentry cache hash table entries: 65536 (order: 6, 262144 bytes)
    Inode-cache hash table entries: 32768 (order: 5, 131072 bytes)
    Memory: 515160K/524288K available (2880K kernel code, 111K rwdata, 1048K rodata, 172K init, 197K bss, 9128K reserved, 0K cma-reserved, 0K highmem)
    Virtual kernel memory layout:
        vector  : 0xffff0000 - 0xffff1000   (   4 kB)
        fixmap  : 0xffc00000 - 0xfff00000   (3072 kB)
        vmalloc : 0xe0800000 - 0xff800000   ( 496 MB)
        lowmem  : 0xc0000000 - 0xe0000000   ( 512 MB)
        pkmap   : 0xbfe00000 - 0xc0000000   (   2 MB)
        modules : 0xbf000000 - 0xbfe00000   (  14 MB)
          .text : 0xc0008000 - 0xc03de384   (3929 kB)
          .init : 0xc03df000 - 0xc040a000   ( 172 kB)
          .data : 0xc040a000 - 0xc0425ec0   ( 112 kB)
           .bss : 0xc0425ec0 - 0xc04572f0   ( 198 kB)
    NR_IRQS:16 nr_irqs:16 16
    IRQ: Found an INTC at 0xfa200000 (revision 5.0) with 128 interrupts
    OMAP clockevent source: timer2 at 24000000 Hz
    sched_clock: 32 bits at 24MHz, resolution 41ns, wraps every 89478484971ns
    clocksource: timer1: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 79635851949 ns
    OMAP clocksource: timer1 at 24000000 Hz
    Calibrating delay loop... 996.14 BogoMIPS (lpj=4980736)
    pid_max: default: 32768 minimum: 301
    Mount-cache hash table entries: 1024 (order: 0, 4096 bytes)
    Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes)
    CPU: Testing write buffer coherency: ok
    Setting up static identity map for 0x80008200 - 0x80008258
    devtmpfs: initialized
    VFP support v0.3: implementor 41 architecture 3 part 30 variant c rev 3
    omap_hwmod: tptc0 using broken dt data from edma
    omap_hwmod: tptc1 using broken dt data from edma
    omap_hwmod: tptc2 using broken dt data from edma
    omap_hwmod: debugss: _wait_target_disable failed
    clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
    pinctrl core: initialized pinctrl subsystem
    NET: Registered protocol family 16
    DMA: preallocated 256 KiB pool for atomic coherent allocations
    cpuidle: using governor ladder
    cpuidle: using governor menu
    OMAP GPIO hardware version 0.1
    edma 49000000.edma: Legacy memcpy is enabled, things might not work
    edma 49000000.edma: TI EDMA DMA engine driver
    usbcore: registered new interface driver usbfs
    usbcore: registered new interface driver hub
    usbcore: registered new device driver usb
    omap_i2c 44e0b000.i2c: could not find pctldev for node /ocp/l4_wkup@44c00000/scm@210000/pinmux@800/pinmux_i2c0_pins, deferring probe
    omap_i2c 4802a000.i2c: could not find pctldev for node /ocp/l4_wkup@44c00000/scm@210000/pinmux@800/pinmux_i2c1_pins, deferring probe
    EDAC MC: Ver: 3.0.0
    clocksource: Switched to clocksource timer1
    NET: Registered protocol family 2
    TCP established hash table entries: 4096 (order: 2, 16384 bytes)
    TCP bind hash table entries: 4096 (order: 2, 16384 bytes)
    TCP: Hash tables configured (established 4096 bind 4096)
    UDP hash table entries: 256 (order: 0, 4096 bytes)
    UDP-Lite hash table entries: 256 (order: 0, 4096 bytes)
    NET: Registered protocol family 1
    futex hash table entries: 256 (order: -1, 3072 bytes)
    VFS: Disk quotas dquot_6.6.0
    VFS: Dquot-cache hash table entries: 1024 (order 0, 4096 bytes)
    Block layer SCSI generic (bsg) driver version 0.4 loaded (major 251)
    io scheduler noop registered
    io scheduler deadline registered
    io scheduler cfq registered (default)
    pinctrl-single 44e10800.pinmux: 142 pins at pa f9e10800 size 568
    Serial: 8250/16550 driver, 6 ports, IRQ sharing disabled
    console [ttyS0] disabled
    44e09000.serial: ttyS0 at MMIO 0x44e09000 (irq = 155, base_baud = 3000000) is a 8250
    console [ttyS0] enabled
    48022000.serial: ttyS1 at MMIO 0x48022000 (irq = 156, base_baud = 3000000) is a 8250
    48024000.serial: ttyS2 at MMIO 0x48024000 (irq = 157, base_baud = 3000000) is a 8250
    481a6000.serial: ttyS3 at MMIO 0x481a6000 (irq = 158, base_baud = 3000000) is a 8250
    481a8000.serial: ttyS4 at MMIO 0x481a8000 (irq = 159, base_baud = 3000000) is a 8250
    481aa000.serial: ttyS5 at MMIO 0x481aa000 (irq = 160, base_baud = 3000000) is a 8250
    davinci_mdio 4a101000.mdio: davinci mdio revision 1.6
    davinci_mdio 4a101000.mdio: detected phy mask fffffffe
    libphy: 4a101000.mdio: probed
    davinci_mdio 4a101000.mdio: phy[0]: device 4a101000.mdio:00, driver SMSC LAN8710/LAN8720
    cpsw 4a100000.ethernet: Detected MACID = 80:30:dc:90:d5:4f
    SLIP: version 0.8.4-NET3.019-NEWTTY (dynamic channels, max=256) (6 bit encapsulation enabled).
    CSLIP: code copyright 1989 Regents of the University of California.
    SLIP linefill/keepalive option.
    ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
    ehci-omap: OMAP-EHCI Host Controller driver
    i2c /dev entries driver
    omap_wdt: OMAP Watchdog Timer Rev 0x01: initial timeout 60 sec
    softdog: Software Watchdog Timer: 0.08 initialized. soft_noboot=0 soft_margin=60 sec soft_panic=0 (nowayout=0)
    omap_hsmmc 48060000.mmc: Got CD GPIO
    mmc0: mmc_rescan_try_freq: trying to init card at 400000 Hz
    mmc0: host does not support reading read-only switch, assuming write-enable
    ledtrig-cpu: registered to indicate activity on CPUs
    ti_am3359-tscadc 44e0d000.tscadc: chan 0 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 1 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 2 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 3 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 4 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 5 step_avg truncating to 16
    ti_am3359-tscadc 44e0d000.tscadc: chan 6 step_avg truncating to 16
    mmc0: new high speed SDHC card at address 5048
    mmcblk0: mmc0:5048 SD16G 14.5 GiB
    NET: Registered protocol family 17
    sctp: Hash tables configured (established 512 bind 1024)
    omap_voltage_late_init: Voltage driver support not added
    ThumbEE CPU extension supported.
     mmcblk0: p1 p2 p3 p4
    mmc1: mmc_rescan_try_freq: trying to init card at 400000 Hz
    tps65217 0-0024: TPS65217 ID 0xe version 1.2
    omap_i2c 44e0b000.i2c: bus 0 rev0.11 at 400 kHz
    rtc-m41t80 1-0068: read error
    mmc1: MAN_BKOPS_EN bit is not set
    rtc-m41t80 1-0068: rtc core: registered m41t81s as rtc0
    rtc-m41t80 1-0068: Can't clear HT bit
    rtc-m41t80: probe of 1-0068 failed with error -121
    omap_i2c 4802a000.i2c: bus 1 rev0.11 at 100 kHz
    hctosys: unable to open rtc device (rtc0)
    EXT4-fs (mmcblk0p2): INFO: recovery required on readonly filesystem
    EXT4-fs (mmcblk0p2): write access will be enabled during recovery
    mmc1: new high speed MMC card at address 0001
    mmcblk1: mmc1:0001 S10004 3.56 GiB
    mmcblk1boot0: mmc1:0001 S10004 partition 1 4.00 MiB
    mmcblk1boot1: mmc1:0001 S10004 partition 2 4.00 MiB
     mmcblk1: p1 p2 p3 p4
    EXT4-fs (mmcblk0p2): recovery complete
    EXT4-fs (mmcblk0p2): mounted filesystem with ordered data mode. Opts: (null)
    VFS: Mounted root (ext4 filesystem) readonly on device 179:2.
    devtmpfs: mounted
    Freeing unused kernel memory: 172K (c03df000 - c040a000)
    fsck 1.44.5 (15-Dec-2018)
    CP437: Invalid argument
    fsck.fat 4.1 (2017-01-24)
    /dev/mmcblk0p1: 7 files, 1489/8167 clusters
    fsck 1.44.5 (15-Dec-2018)
    rootfs: clean, 3508/20480 files, 76937/81920 blocks
    fsck 1.44.5 (15-Dec-2018)
    rootfs: recovering journal
    rootfs: clean, 3508/20480 files, 76935/81920 blocks
    fsck 1.44.5 (15-Dec-2018)
    /dev/mmcblk1p3 contains a file system with errors, check forced.
    /dev/mmcblk1p3: Entry 'uboot.env' in / (2) has filetype set.
    CLEARED.
    /dev/mmcblk1p3: 12/128 files (0.0% non-contiguous), 1073/2048 blocks
    fsck 1.44.5 (15-Dec-2018)
    /dev/mmcblk1p4: clean, 30/196608 files, 46769/3145728 blocks
    fsck 1.44.5 (15-Dec-2018)
    /dev/mmcblk0p3: recovering journal
    /dev/mmcblk0p3: clean, 11/128 files, 1063/2048 blocks
    fsck 1.44.5 (15-Dec-2018)
    /dev/mmcblk0p4: clean, 30/196608 files, 46769/3145728 blocks
    random: nonblocking pool is initialized
    EXT4-fs (mmcblk0p2): re-mounted. Opts: (null)
    EXT4-fs (mmcblk0p4): mounted filesystem with ordered data mode. Opts: (null)
    mount: /home/microsd/: can't find PARTUUID=41555820-02.
    mount: /upgrade: can't find PARTUUID=41555820-01.
    EXT4-fs (mmcblk0p3): mounted filesystem with ordered data mode. Opts: (null)
    Starting rsyslogd: OK
    Initializing random number generator... done.
    Starting network: net eth0: initializing cpsw version 1.12 (0)
    net eth0: phy found : id is : 0x7c0f1
    libphy: PHY 4a101000.mdio:01 not found
    net eth0: phy 4a101000.mdio:01 not found on slave 1
    OK
    Starting dropbear sshd: OK
    Starting kubos-app-service:
    OK
    Starting file-service:
    OK
    Starting shell-service:
    OK
    Starting telemetry-service:
    OK
    Starting mai400-service:
    OK
    Starting monitor-service:
    OK
    Starting novatel-oem6-service:
    OK
    Starting scheduler-service:
    OK
    Cannot open /envar/uboot.env: No such file or directory
    Error: environment not initialized
    Cannot open /envar/uboot.env: No such file or directory
    Error: environment not initialized
    Starting Monit 5.25.2 daemon with http interface at [*]:7000
    'Kubos' Monit 5.25.2 started

    Welcome to Kubos Linux
    Kubos login: 'kubos-mai400' process is not running
    'kubos-mai400' trying to restart
    'kubos-mai400' start: '/etc/init.d/S90kubos-mai400 start'
    Traceback (most recent call last):
      File "/usr/sbin/pumpkin-mcu-service/service.py", line 36, in <module>
        schema.MODULES = c.raw['modules']
    KeyError: 'modules'

    Welcome to Kubos Linux
    Kubos login:

Once booted, the KubOS Image can be installed now


.. _install-kubos-from-image:

+++++++++++++++++++++++++++++++++++
Installing KubOS from Install Image
+++++++++++++++++++++++++++++++++++

Once booted, simply log-in as the ``root`` user with the password ``Kubos123``.
After logging in, run the ``install-os`` command to install the operating system locally on the BBB.
Answer ``Y`` to the question and wait ~15 minutes for the install to complete.
The output should be similar to ::

    /home/kubos # install-os
    ** Warning ** This script will wipe out the current contents of the eMMC
    Are you sure you want to proceed? (y/N)
    y
    OS Install: Pausing monitoring
    'Kubos' unmonitor on user request
    'kubos-app-service' unmonitor on user request
    'file-service' unmonitor on user request
    'kubos-mai400' unmonitor on user request
    'kubos-monitor' unmonitor on user request
    'kubos-novatel-oem6' unmonitor on user request
    'kubos-shell-service' unmonitor on user request
    'kubos-telemetry-db' unmonitor on user request
    'scheduler-service' unmonitor on user request
    'monit' unmonitor on user request
    Awakened by User defined signal 1
    Monit daemon with PID 553 awakened
    OS Install: Stopping running services
    'monit' unmonitor action done
    'scheduler-service' unmonitor action done
    'kubos-telemetry-db' unmonitor action done
    'kubos-shell-service' unmonitor action done
    'kubos-novatel-oem6' unmonitor action done
    'kubos-monitor' unmonitor action done
    'kubos-mai400' unmonitor action done
    'file-service' unmonitor action done
    'kubos-app-service' unmonitor action done
    'Kubos' unmonitor action done
    sh: can't kill pid 576: No such process
    killall: python: no process killed
    Monit daemon with pid [553] stopped
    'Kubos' Monit 5.25.2 stopped
    Starting Monit 5.25.2 daemon with http interface at [*]:7000
    'Kubos' Monit 5.25.2 started
    OS Install: Prepping the file system
    umount: /home: umount failed: Operation not permitted.
    OS Install: Copying SD card contents over to eMMC. This process should take 10-15 minutes to complete
    3.56GiB 0:12:54 [4.71MiB/s] [================================>] 100%
    7471104+0 records in
    7471104+0 records out
    7471104+0 records in
    7471104+0 records out
    3825205248 bytes (3.6GB) copied, 779.085612 seconds, 4.7MB/s
    /home/kubos #

Once finished, flash the Auxiliary SD card image onto the install MicroSD (or new MicroSD) and place that into the MicroSD card slot.
See `Install the Auxilary Image <https://docs.kubos.com/1.21.0/obc-docs/mbm2/installing-linux-mbm2.html#install-the-auxiliary-image>`_ for more information.

The KubOS image is installed now, and can be used to develop for the KubOS framework.