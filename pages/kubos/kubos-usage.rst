.. _kubos-usage-architecture:

----------------------------
KubOS Usage and Architecture
----------------------------

KubOS is a very open and free architecture; it allows for the user to utilize a linux-based environment in a spacecraft.
Typically a KubOS distribution is broken down into a set of:

* Libraries and APIs
* Services
* Applications

Libraries and API's tend to be used by Services, which are in turn used by KubOS applications.
A typical spacecraft OS image will have many different services and applications controlling the CONOPS of the spacecraft.

Each one of these applications/services/libraries are abstracted as `buildroot packages <https://buildroot.org/downloads/manual/manual.html#adding-packages>`_ hosted within the ``kubos-linux-build/packages`` directory.

KubOS Packages
==============

KubOS uses the concept of buildroot packages to represent libraries/services/applications installed as part of the KubOS image.
APIs/Libraries typically include:

* Base support for low-level support of applications/services
* Direct I2C/UART/Ethernet/UDP interface to hardware/software present on Bus/Linux
* Meant to be abstracted as part of a KubOS application/service to the end-user

.. note:: KubOS provides a document on `mission development <https://docs.kubos.com/1.21.0/mission-dev/index.html>`_ for more information on how to integrate KubOS as part of the CONOPS of the user.

Example of KubOS Package
************************

The ``pumpkin-mcu-api`` as included in the `KubOS FSW <https://github.com/kubos/kubos/tree/master/apis/pumpkin-mcu-api>`_ is an example of the API used to interface with the Pumpkin SupMCU modules.
This is used by the ``pumpkin-mcu-service`` to directly interface with the Pumpkin SupMCU modules via I2C.
There are two main files inside of the `KubOS linux build <https://gitlab.com/pumpkin-space-systems/public/kubos-linux-build>`_ for the `pumpkin-mcu-api <https://gitlab.com/pumpkin-space-systems/public/kubos-linux-build/-/tree/master/package/kubos/kubos-pumpkin-mcu-api>`_ buildroot package.

* ``Config.in`` - The Buildrooot ``menuconfig`` configuration modules
* ``<name of package folder>.mk`` - The ``Makefile`` defining the environment variables and build script used to install the package. In the ``pumpkin-mcu-api`` the name of this file is ``pumpkin-mcu-api.mk``

The ``pumpkin-mcu-api`` is an example of a Python dependency for KubOS. For Rust dependencies, see the `MAI400 API <https://github.com/kubos/kubos/tree/master/apis/mai400-api>`_ for a Rust example.

Example of KubOS Service
************************

Services are the main unit of hardware <-> software functionality in KubOS.
Services typically tend to cover core OS functionality such as scheduling as well.
A KubOS service is:

* Typically run on-demand as requests from the applications
* Meant to serve as the interface to hardware/OS-level software
* Always running from start of KubOS

The ``kubos-pumpkin-mcu`` service as `included here <https://gitlab.com/pumpkin-space-systems/public/kubos-linux-build/-/tree/master/package/kubos/kubos-pumpkin-mcu>`_ defines a:

* ``Config.in`` - The buildroot ``menuconfig`` configuration options
* ``kubos-pumpkin-mcu`` - The script to execute the KubOS service as an init application
* ``kubos-pumpkin-mcu.mk`` - The ``Makefile`` defining the environment and build script used to install the service.

Services are dependencies of KubOS applications

Example of KubOS Application
****************************

Applications are the main unit of CONOPS for a KubOS-based architecture.
These are used to:

* Run the spacecraft and react to the current situation on the spacecraft
* Facilitate day-to-day tasks such as beconing spacecraft telemetry and state of health
* React to commands directly from the ground-station

An example of a KubOS application is the ``beacon-app`` as included in the `Apollo Fusion <https://github.com/kubos/apollo-fusion/>`_.
The contents are:

* ``src`` - Folder containing the Rust executable code of the application
* ``Cargo.toml`` - The Rust cargo (package) configuration file
* ``manifest.toml`` - The metadata about the application author and information
