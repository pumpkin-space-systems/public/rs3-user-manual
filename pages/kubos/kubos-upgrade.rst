--------------
KubOS Upgrades
--------------

KubOS upgrades are able to:

* Integrate new services/applications into an existing KubOS install
* Update existing service/application code
* Update system configuration files in ``/etc`` (and any other folder besides those listed below) on the device

KubOS upgrades WILL not:

* Edit/add/delete existing user files in ``/home/kubos``
* Change existing configuration and log  files ``/home/system``
* Change any upgrades in the ``/upgrade`` directory.

.. note:: Full documentation can be found in `KubOS Upgrade Documentation <https://docs.kubos.com/1.21.0/ecosystem/linux-docs/kubos-linux-upgrade.html>`_

Creation and Usage of KubOS Upgrades
====================================

Creation of a KubOS upgrade .itb file can be done once the KubOS SDK has been installed and the KubOS image has been created.
See the :doc:`KubOS SDK Documentation </pages/kubos/kubos-sdk>` for more information for SDK install and image creation.

Creation of KubOS Upgrade
*************************

A `KubOS Upgrade <https://docs.kubos.com/1.21.0/ecosystem/linux-docs/kubos-linux-upgrade.html>`_ is a series of patch files for upgrading the OS packaged into an image tree blob (itb) file.
Creating an upgrade file is straightforward from the ``kubos-package.sh`` script. The process for creating an update file is ::

    $ vagrant ssh
    ... wait for Vagrant to connect ...
    $ cd ~/kubos-linux/kubos-linux-build/tools
    $ BINARIES_DIR=../../buildroot-2019.02.2/output/images/ ./kubos-package.sh -t pumpkin-mbm2 -b 1.5 -v <Name of upgrade file>

.. attention:: The ``-b 1.5`` parameter is mandatory; this is needed else the package script won't run successfully

Once that script has finished, the file can be directly copied over via ``scp`` command or similar to the ``upgrade`` partition of the target.

Usage of KubOS Upgrade
**********************

Usage of the KubOS upgrade is simple, the user must:

1. Turn on the RS3/FlatSat unit and wait for the unit to boot and be able to login via SSH
2. Transfer the ``kpack-<name of upgrade>.itb`` file to the target device
3. Set the ``kubos_updatefile`` environment variable to the ``kpack-<name of upgrade>.itb`` file via the ``fw_setenv`` command
4. Reboot the device and wait for the upgrade to be applied

To transfer and start the upgrade, the user must open the Vagrant VM and run the following commands from the Vagrant commandline ::

    $ scp kubos-linux-build/tools/kpack-<Name of upgrade file>.itb kubos@<upgrade target IP address>:/upgrade
    ... enter password ...
    $ ssh kubos@<upgrade target IP address>
    ... enter password ...
    $ fw_setenv kubos_updatefile kpack-<Name of upgrade file>.itb
    $ reboot

Once rebooted, this will trigger the KubOS Upgrade process.

Simply ping the machine and wait for the RS3 unit to come back online, then login and proceed to test the new KubOS upgrade.