---------------------------
Solar Power Simulator (SPS)
---------------------------

.. SPS PICTURE

For FlatSat's equipped with an EPSM unit, an optional Solar Power Simulator (SPS) can be purchased to provide simulated solar panel input to the EPSM unit.
The SPS contains one or more HP GPIB PSU's connected to a single Prologix GPIB adapter. The PSU's are mounted inside of a portable rack-mount case,
and is controlled from any device capable of running Python 3.7+.

Hardware Interface
******************

The SPS is connected to the EPSM via four SPS-to-SAI harnesses. The connections *must* be made to the proper numbered
connectors on the front-panel of the SPS unit. See annotation below for connections:

.. PICTURE OF CONNECTION WITH ANNOTATION

.. caution:: Please exercise caution when connecting to the EPSM. Power off all devices, wear an ESD-protective device and work on an ESD-safe surface.
    Failure to do so may result to damage to the SPS or connected devices.

Software Interface
******************

.. PICTURE OF INTERACTIVE INTERFACE

The software for the solar panel simulator can be run on any device capable of running Python 3.7+, as the requirement is only an Ethernet or USB
connection to the Prologix controller. The Solar Simulator includes two different modes:

* Interactive - Loads a configuration TOML file and enters an interactive mode to allow the user to adjust the parameters during runtime.
* Daemon - Loads a configuration TOML file and runs without any user interaction.
  A keyboard interrupt or -SIGTERM will safely shutdown the process, allowing the power simulator to safely shutdown.

For Windows platforms, a stand-alone binary can be provided. Other platforms should use a ``pipenv`` environment or similar to run the Solar Power Simulator.

Configuring the Solar Simulator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The configuration of the solar simulator is controlled via TOML configuration files. Possible parameters include:

* Configuration of Prologix controller type (USB or Ethernet) and endpoint.
* Configuration of HP GPIB PSU's to use with associated GPIB address

    * See configuration fragment below for complete list of supported PSU's

* Configuration of channel groups and simulation type:

    * Channel groups are a listing of multiple channels to associate with a simulation type
    * There are two modes supported currently: `sine` and `static`
    * See the configuration example for full details

Configuration example ::

    [sunsim]
    # --- Prologix Configuration ---
    # The type of Prologix adapter used.
    # If `ethernet`:
    #   The `endpoint` is set to the IP Address of the Prologix adapter.
    #   Use the `netfinder` tool from Prologix to locate the adapter on your local network
    #   See: http://prologix.biz/resources.html
    #
    # If `usb`:
    #   The `endpoint` is set to the device path `/dev/ttyUSB#` for Unix-based operating systems
    #   For Windows systems, the Virtual COM Port # is used, e.g. `COM#` in Device Manager
    connection_type = "ethernet"
    # The IP Address or device path to the Prologix adapter
    endpoint = "10.30.3.20"

    # --- PSU List ---
    # The list of the power supply units to use for simulation on the solar simulator application.
    # Each entry uses the Array of tables syntax for the TOML configuration language, see: https://toml.io/en/v1.0.0-rc.2#array-of-tables
    # for more information.
    #
    # Note the list of Supported PSU `model`s is:
    #       Multi-channel HP PSUs:
    #           HP6621A - 2 Channels (20V/10A)
    #           HP6622A - 2 Channels (50V/4A)
    #           HP6623A - 2 Channels (20V/10A), 1 Channel (50V/2A)
    #           HP6624A - 2 Channels (20V/5A), 2 Channels (50V/2A)
    #           HP6627A - 4 Channels (50V/2A)
    #           HP6629A - 4 Channels (50V/1A)
    #       Single-channel HP PSUs:
    #           HP6632A - 20V/5A
    #           HP6633A - 50V/2A
    #           HP6634A - 100V/1A
    #           HP6038A - 60V/10A
    # The `gpib_addr` is the assigned GPIB address for the PSU
    # To add another PSU, simply uncomment/copy a `[[sunsim.psu]]` section and fill in the appropriate parameters
    [[sunsim.psu]]
    # The GPIB Address of the power supply
    gpib_addr = 1
    # The model of HP PSU to use. If a single-channel PSU is used, the `channel_number` used below shall be `1`
    model = "HP6629A"

    # --- Simulator Groups ---
    [[sunsim.group]]
    # A list of GPIB Addresses -> Channels to map as SAI inputs on the EPSM.
    # The `gpib_address` is the GPIB address of the HP instrument
    # The `channel_number` is the channel number of whats hooked up to the SAI input.
    # Use the syntax `{ gpib_address = #, channel_number = # }`
    channels = [
        { gpib_address = 1, channel_number = 1},
        { gpib_address = 1, channel_number = 2},
    ]
    # The type of simulation to use, currently two simulation types are available:
    #   - `sine`: Simulation of a spinning spacecraft. Assumes S/C rotating at `deg_sec`, starting at `start_degree`, with a
    #             `phase_offset` applied between each channel (SAI Input), and adding a `global_phase_offset` to all channels.
    #             The output is varied by current, with the current being scaled between `min/max_current` and the output voltage
    #             set to `voltage`. The maximum current is set when the degree for the channel is set to 0.
    #   - `static`: The PSU sets each channel to `voltage` at `current`.
    sim_type = "sine"

    # Simulation parameters for the `sine` type of channel group
    [sunsim.group.parameters]
    # The initial degrees per second to set the simulation to. Can be changed via CLI interface by selecting group
    # and pressing +/-
    deg_sec = 10
    # The degree to start the simulation at
    start_degree = 0
    # The phase offset to apply to every channel [e.g. sin(current_degree + global_phase_offset)]
    global_phase_offset = 0
    # The phase offset to apply between each channel
    phase_offset = 180
    # The minimum current to apply to a given channel
    min_current = 0.0
    # The maximum current to apply to a given channel
    max_current = 1.0
    # The voltage to use for the channels
    voltage = 40.0

    [[sunsim.group]]
    # Channels for the sun simulator channel group
    channels = [
        { gpib_address = 1, channel_number = 3},
        { gpib_address = 1, channel_number = 4},
    ]
    # Set the PSU to a static current and voltage
    sim_type = "static"

    [sunsim.group.parameters]
    # The current to set the PSU to
    current = 1.0
    # The voltage to set the PSU to
    voltage = 40.0

Running the Solar Simulator
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note:: Before running the Solar Simulator, configure the solar simulator and save the file to a known location on the disk.

Simply run the ``solar-simulator`` application from the command-line like below ::

    $ solar-simulator /path/to/config-file.toml
    # To launch the simulator in interactive mode, use
    $ solar-simulator -i /path/to/config-file.toml

Setting up ``pipenv`` environment for Solar Simulator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Pipenv <https://pipenv.pypa.io/en/latest/>`_ is used to manage virtual environments for Python applications.
This is highly recommended to manage application dependencies for Python development and applications.
Once Python 3.7+ is installed, the installation of ``pipenv`` is simple. Simply run ::

    # If only installing for user
    $ pip install --user pipenv
    # For system installation, use
    $ pip install pipenv
    # See https://pipenv.pypa.io/en/latest/ for more information.

Once ``pipenv`` is installed, extract the Solar Simulator scripts into a separate folder and run the following commands to install the required dependencies ::

    $ cd /path/to/solar-simulator
    # Initialize the environment, install requirements, and install package into environment
    $ pipenv --three
    $ pipenv run pip install -r requirements.txt
    $ pipenv run pip install .

To run the solar power simulator from the ``pipenv`` environment, run ::

    $ pipenv run solar-simulator /path/to/config.toml
