---------------------
RS3/FlatSat Libraries
---------------------

Pumpkin provides two libraries to interface directly with the SupMCU modules via I2C with Python directly on the BBB.
These include:

* ``pumpkin-supmcu`` - Pure Python library to handle the business logic of interfacing with the SupMCU modules over I2C
* ``pumpkin-supmcu-kubos`` - Python library to implement the ``I2CMaster`` class for KubOS

.. note::
    A future update will integrate the ``pumpkin-supmcu`` library as part of the ``pumpkin-mcu-service`` KubOS Service to facilitate the interface inside of KubOS to Pumpkin modules.
    Stay tuned for the latest updates.

.. _pumpkin-supmcu:

``pumpkin-supmcu``
==================

The ``pumpkin-supmcu`` library is a small library to decode the telemetry packets from the SupMCU modules on the SUPERNOVA CSK bus or the RS3 unit.
Its feature set includes:

* Discovery of telemetry on any given SupMCU module
* Definition of abstract interfaces to give access to an I2C bus
* Parsing/decoding packets received from SupMCU modules over I2C
* Validation of parsing format strings received from SupMCU modules
* Serial interface for telemetry injection

For more information, visit the `pumpkin-supmcu documentation <https://pumpkin-supmcu.readthedocs.io/en/latest/>`_.

``pumpkin-supmcu-kubos``
************************

The ``pumpkin-supmcu-kubos`` is an implementation of the `I2CMaster protocol <https://pumpkin-supmcu.readthedocs.io/en/latest/api/i2c.html#pumpkin_supmcu.i2c.I2CMaster>`_ for KubOS.

Its usage is simple; import the ``pumpkin_supmcu.kubos.KubosI2CMaster`` class and instantiate it with ``KubosI2CMaster(1)`` from any application.

Injection Example
=================

The following example injects all of the telemetry inside of a telemetry set generated from ``pumgen`` named ``rs3-tlm.json`` into the modules present on the KubOS bus ::

    #!/usr/bin/env python3
    """
    Injects a telemetry set from `pumqry` or `pumgen`, showing the telemetry before and after injection.
    """
    from pathlib import Path

    from pumpkin_supmcu.supmcu import set_values, get_values, SupMCUSerialMaster, get_version_string, datatype_to_supmcu_fmt_char
    from pumpkin_supmcu.kubos import I2CKubosMaster
    from putdig.common import compare_versions, import_bus_telemetry

    KUBOS_I2C_BUS = 1


    def get_format_string(tlm_items):
        """
        Gets a format string for `get_values` from a list of telemetry items.

        :param tlm_itmes: The list of telemetry items to get the format string for.
        :return: The format string for the telemetry items.
        """
        return ",".join(datatype_to_supmcu_fmt_char(tlm.data_type) for tlm in tlm_items)


    def print_tlm(items):
        """
        Prints out each telemetry item.

        :param items: The list of tuples of names, items to print out.
        """
        for idx, item in enumerate(items):
            print(f"Item #{idx}")
            print(f"\tvalue={item.string_value}")
            print()


    def inject(master, modules):
        """
        Injects the telemetry from `modules` using the `master` given.

        :param master: The I2C master to use.
        :param modules: The Telemetry to inject
        """
        for mod in modules:
            # Get version of the module, and make sure the version matches the generated set
            version = get_version_string(master, mod.address, mod.cmd_name)
            if compare_versions(version, mod.version):
                print("*"*80)
                print(f"Injecting Telemetry for: {version}")
                print("*"*80)
                for telem in mod.module_telemetry:
                    # Check to make sure the telemetry item is simulatable on the module.
                    if telem.simulatable:
                        curr_value = get_values(master, mod.address, mod.cmd_name, telem.idx, get_format_string(telem.sup_telemetry.items))
                        set_values(master, mod.address, mod.cmd_name, telem.idx, telem.sup_telemetry.items)
                        after_val = get_values(master, mod.address, mod.cmd_name, telem.idx, get_format_string(telem.sup_telemetry.items))
                        print("-"*80)
                        print(f"Item: {telem.name}\nBefore:")
                        print_tlm(curr_value)
                        print("After:")
                        print_tlm(after_val)
                        print("-"*80)
            else:
                raise ValueError(f"Module version mismatch.  Expected value '{mod.version}' != '{version}'")


    def main(tlm_path):
        """
        Main method for injecting telemetry
        """
        tlm_path = Path(tlm_path)
        tlm = import_bus_telemetry(tlm_path)
        master = I2CKubosMaster(KUBOS_I2C_BUS)
        inject(master, tlm)


    if __name__ == "__main__":
        main('./rs3-tlm.json')

This will:

1. Check the version of the modules against the version specified inside of the ``rs3-tlm.json`` specified. If the module is not present/doesn't match version, then an error is raised
2. Show the value of the telemetry item before injection
3. Inject the telemetry value
4. Show the value of the telemetry item after injection

To use, simply make the script executable via ``chmod +x inject.py`` and run ``./inject.py`` from the BBB command-line.