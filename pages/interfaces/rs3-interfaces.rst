.. _rs3-electrical-interfaces:

--------------
RS3 Interfaces
--------------

On the RS3 unit, there are several built-in interfaces including:

* 4x 100/10 Ethernet interfaces
* 4x RS232/RS422 DB-9 interfaces
* 4x USB Type-B interfaces
    + 2x for QSM modules Debug UART
    + 1x for BBB Mini Type-B Connection
    + 1x reserved for future use
* 28V DC output
* 1x USB Type A for BBB Type-A connector
* 1x 2x5 Box connector for I2C

The front of the RS3 unit includes connections for Ethernet, 2x5 Box connector and USB Type-B to QSM modules:

.. image:: /img/front-panel-io.JPG

Back of the RS3 unit includes connections for RS232/RS422, Ethernet, 28V DC output, and USB Type-A to BBB:

.. image:: /img/rear-panel-io.JPG

See the RS3 datasheet for more information on electrical connections.

Connectors for BBB
==================

The following cover the electrical connections that are directly used by the BBB including:

* RS232/RS422 connections to the CSK bus
* USB Type-A connection to BBB
* USB Type-B connection to BBB

The USB Type-A connection on the back is directly connected to the host USB slot on the BBB

The USB Type-B connection is routed to the Mini Type-B connection on the BBB.
The current implementation of the KubOS Linux does not break out this connection as a communication interface.

The 4x RS232/RS422 connections to the BBB/CSK bus from left to right are:

* ``BBB UART5/CSK UART0`` - Connection to ``/dev/ttyS5`` on BBB (CSK UART0)
* ``BBB UART1/CSK UART1`` - Connection to ``/dev/ttyS1`` on BBB (CSK UART1)
* ``BBB UART2/CSK UART2`` - Connection to ``/dev/ttyS2`` on BBB (CSK UART2)
* ``BBB UART4/CSK UART3`` - Connection to ``/dev/ttyS4`` on BBB (CSK UART3)

.. note:: ``BBB UART3/CSK UART4`` (``/dev/ttyS3``) is a TX-only interface, and is only available directly on the CSK header.

.. note::
    If the ``MAI-401`` simulation module is present inside of the RS3, then the connection on the rear for ``BBB UART5/CSK UART0`` is disconnected from the port.
    This is used internally for communication to the ``MAI-401`` Simulator module.

.. note::
    RS422 connections are only present upon request of the customer.
    All DB9 connections on the back are RS232 otherwise.

Connectors for RS3
==================

The connections for RS3 include:

* 3x 10/100 Ethernet connections (1 Ethernet connection is reserved internally for the MBM2+BBB)
* 4x USB Type-B connections
* 1x 2x5 Box connector for I2C

The 10/100 Ethernet connections are routed to a switch inside of the RS3 for connecting the local network to the RS3 unit.
All of these Ethernet connections are meant to be used by the end-user to interface directly with the RS3 unit or with payloads connected to the RS3 unit.

The 2 USB Type-B connections are used for Debug and firmware programming of the QSM modules inside of the RS3 unit.
Contact Pumpkin directly for instructions on how to program the QSM modules.
