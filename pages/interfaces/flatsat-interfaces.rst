------------------
FlatSat Interfaces
------------------

On a particular FlatSat there are only two required modules:

* Motherboard Module 2 (MBM2)
* A CSK compatible power supply:

    + Electrical Power System (EPSM)
    + Desktop Cubesat Power Supply (DCPS)

A FlatSat can interface/host a variety of modules including:

* Payload Interface Module (PIM)
* Radio Host Module 1/2 (RHM1/2)
* ADCS Interface Module 2 (AIM2)
* Bus Interface Module (BIM)
* Solar Interface Module (SIM)
* Battery Module 2 (BM2) [via Harness]
* Battery Switch Module (BSM) [via Harness]
* Remote Ground Service Interface Module (Remote GSIM) [via Harness]

A FlatSat is meant to be an open platform to host various EM modules to represent a FM stack for a SUPERNOVA Spacecraft.

Connections for Remote GSIM
===========================

.. REMOTE GSIM PICTURE ANNOTATED

The Remote GSIM module allows the user to toggle the SEP and RBF inhibits for the EPSM and BM2.
The GSIM exposes the Ethernet connectivity to the bus as well as other ancillary functions (power input/status LED and USB to BBB).

Connections for MBM2
====================

.. MBM2 PICTURE ANNOTATED

The following cover the electrical connections that are directly used by the BBB including:

* UART connections to the CSK bus
* Debug Adapter connection to BBB
* MiniUSB connection to BBB
* Host USB-A on BBB

.. note::
    The Mini Type-B on the BBB is currently not enabled in the current KubOS image.

The 5x UART connections to the BBB/CSK bus are:

* ``BBB UART5/CSK UART0`` - Connection to ``/dev/ttyS5`` on BBB (CSK UART0)
    + TX is ``H1.20``, RX is ``H1.19``, CTS is ``H1.11``, RTS is ``H1.12``
* ``BBB UART1/CSK UART1`` - Connection to ``/dev/ttyS1`` on BBB (CSK UART1)
    + TX is ``H1.18``, RX is ``H1.17``, CTS is ``H1.9``, RTS is ``H1.10``
* ``BBB UART2/CSK UART2`` - Connection to ``/dev/ttyS2`` on BBB (CSK UART2)
    + TX is ``H1.8``, RX is ``H1.7``
* ``BBB UART4/CSK UART3`` - Connection to ``/dev/ttyS4`` on BBB (CSK UART3)
    + TX is ``H2.16``, RX is ``H2.15``.
* ``BBB UART3/CSK UART4`` - Connection to ``/dev/ttyS3`` on BBB (CSK UART4)
    + TX is ``H1.5``

.. note:: ``BBB UART3/CSK UART4`` (``/dev/ttyS3``) is a TX-only interface.

.. note:: If the MBM2 Revision F is used, and UART1 and/or UART2 is selected to be on the RS-422 interface, the corresponding CSK connections are NOT present in the system.

    This is dependent on customer configuration, see FlatSat configuration sheet in delivery for more information.

See the MBM2 Datasheet for more information.

Connectors for DCPS
===================

.. DCPS PICTURES ANNOTATED

The connections for DCPS include:

* 1x Mini-XLR input for 24V power
* 2x Harwin M80 connector for BM2/BSM
* 2x sets of Banana-jack plugs:

    + 1x Charging/discharging BM2
    + 1x Alternative input power for the DCPS

* 1x 2x5 Box connector for I2C via TotalPhase Aardvark adapter
* 1x Cadex battery tester power (not used)
* 1x 4-pin FPC connector for UART Debug Console

In configurations without the EPSM, the DCPS powers the FlatSat via the Mini-XLR input and can interface with a BM2/BSM with the ports labeled To/From BM2/BSM respectively.

.. note:: The DCPS does **not** pull power from the BM2 to power the avionics stack, it simply provides an I2C interface to the Battery

See the DCPS Datasheet for more information.

Connectors for the EPSM
=======================

.. EPSM PICTURES ANNOTATED

The connections for the EPSM include:

* 2x Harwin M80 connectors to connect up-to 2 BM2's
* 4x DF-13 connectors to attach up-to 6 solar panel strings

In addition, the EPSM provides power for:

* 3.3V on ``H2.27/H2.28``
* 5V on ``H2.25/H2.26``
* 12V on ``H2.51/H2.52`` (dependent on configuration)
* AUX (28V by default) on ``H2.45/H2.46``

    * Optionally, the EPSM can be configured to expose BAT1 or BAT2 as VBATT on the CSK Bus in-place of AUX

Connectors for the PIM
======================

.. PIM PICTURES ANNOTATED

The connections for the PIM include:

* 4x power ports for payloads from 12V, 5V, 3.3V, and VBATT (depending on customer configuration)
* 3x DF13-4 Ethernet ports for 10/100 Ethernet connectivity to Payloads/BBB
* 1x DF13-8 Ethernet port for 10/100 Ethernet connectivity to the Payloads/BBB
* GPS Passthrough for easier cable routing for GPS antenna
* 1x 4-pin FPC connector for UART Debug Console

The PIM powers various payloads throughout the spacecraft as well as provides Ethernet connectivity to the payloads/spacecraft.

See the PIM Datasheet for more information.

Connectors for the RHM
======================

.. RHM PICTURES ANNOTATED

The connections for the RHM include:

* 1x 4-pin FPC connector for Lithium Radio
* 1x 4-pin FPC connector for UART Debug Console

The Lithium and GlobalStar radios UART channels can be chosen via ``RHM:GS/LI:COMM`` commands. See `RHM1/2 Commands here <https://pumpkin-space-systems.gitlab.io/public/software-reference-manual/pages/rhm.html>`_.

Connectors for the GNSSRM
=========================

.. GNSSRM PICTURES ANNOTATED

The connections for the GPSRM include:

* 1x MicroUSB for OEM719 GPS receiver
* 1x 4-pin FPC connector for UART Debug Console

See the GPSRM datasheet and user manual for more information.

Connectors for the AIM2
=======================

.. AIM2 PICTURES ANNOTATED

The connectors for the AIM2 include:

* 1x MicroUSB for OEM719 GPS receiver
* 1x 4-pin FPC connector for SIM (if present)
* 1x 4-pin FPC connector for UART Debug Console

See the AIM2 datasheet for more information.

Connectors for the BIM
======================

.. PICTURE

The connectors for the BIM include:

* 6x DF13-4 for LM335 temperature sensors
* 3x MicroD connectors for UART/VBATT connectivity
* 1x 4-pin FPC connector for UART Debug Console

See the BIM datasheet for more information.

Connectors for the SIM
======================

The connectors for the SIM include:

* 4x PRM interface ports

See the SIM datasheet for more information.

Connectors for the BM2
======================

The connectors for the BM2 include:

* 1x 4-pin FPC connector for UART Debug Console
* 1x 4-pin FPC connector for Gas Gauge programming interface
* 2x Harwin M80 for power/I2C connectivity

See the BM2 Datasheet for more information.

Connectors for the BSM
======================

The connectors for the BSM include:

* 1x Harwin M80 for input from the BM2
* 4x Harwin M80 for VBATT outputs from BSM
* 1x 5-pin DF-13 connector for external override control

See the BSM datasheet for more information.
