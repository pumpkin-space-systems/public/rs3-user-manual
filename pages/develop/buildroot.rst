.. _app-packing:

-----------------
Buildroot Usage
-----------------

The following sections cover:

* Building the KubOS image in ``buildroot``
* Packaging mission applications in ``buildroot``

=================
KubOS Build Setup
=================

The configuration of the KubOS build system uses the `Buildroot <https://buildroot.org/>`_ toolchain, which is an embedded linux image builder.
The following section details Pumpkin-specific configuration options/packages for the KubOS build system

.. note:: For complete instructions `see KubOS Documentation <https://docs.kubos.com/1.21.0/deep-dive/klb/configuring-kubos.html>`_

.. _initial-build-setup:

Initial Build setup
*******************

To setup the buildroot configuration as well as start building the KubOS OS Image, open a terminal and enter the following commands ::

    $ mkdir kubos-linux
    $ cd kubos-linux
    $ wget https://buildroot.uclibc.org/downloads/buildroot-2019.02.2.tar.gz && tar xvzf buildroot-2019.02.2.tar.gz && rm buildroot-2019.02.2.tar.gz
    $ git clone https://gitlab.com/pumpkin-space-systems/public/kubos-linux-build.git
    $ cd buildroot-2019.02.2

This will:

1. Create a ``kubos-linux`` folder to hold the build files for ``kubos-linux-build``
2. Download, and extract the buildroot toolchain inside of the ``kubos-linux`` folder
3. Clone the extra buildroot configuration files for KubOS

Next, configure the build environment by executing the following, substituting ``<build_defconfig>`` with a specific defconfig name ::

    $ make BR2_EXTERNAL=../kubos-linux-build <build_defconfig>

You can see all the available defconfigs by running ``ls ../kubos-linux-build/configs/``

If you do not yet have a customized defconfig for your build, use ``pumpkin-mbm2_defconfig`` to start: ::

    $ make BR2_EXTERNAL=../kubos-linux-build pumpkin-mbm2_defconfig

Output should be similar to:

.. image:: /img/buildroot-config-1.png

Configuration of KubOS
**********************

The following details the configuration of buildroot packages specific to Pumpkin hardware on KubOS.

Buildroot uses the ``make menuconfig`` command to configure packages present in the output KubOS image.
To enter the configuration menu, simply use ::

    $ cd ~/kubos-linux/buildroot-2019.02.2
    $ make menuconfig

The following UI should be displayed:

.. image:: /img/buildroot-config-2.png

Navigate to ``External Packages``, most configuration will take place under here.
Navigate to ``Kubos Packages`` and enable the following options here using <Y> on the keyboard:

.. image:: /img/buildroot-config-3.png

The user should enable all of the options under the ``Pumpkin MCU Service`` menu (by navigating and pressing <Enter> on the option):

.. image:: /img/buildroot-config-4.png

Once done, simply use the ``< Exit >`` command at the bottom to navigate back by using the <Left Arrow>/<Right Arrow> on keyboard.
Once up one level, select ``Additional Python Packages`` and enable the following options:

.. image:: /img/buildroot-config-5.png

Once done, KubOS should be configured to include the PuTDIG software as well as ``pumpkin_supmcu`` library.

Editing ``pumpkin-mbm2`` Filesystem overlay
*******************************************

When the ``pumpkin-mbm2`` image is built for KubOS, the files contained in ``kubos-linux-build/board/kubos/pumpkin-mbm2/overlay/`` are applied on top of the root filesystem of the KubOS image.
The user can use this overlay folder to add/modify files from the root filesystem.
There are many use cases from adding/removing configuration files for the system or inserting system data.
A recommended configuration change is to assign a different IP address to the ``eth0`` adapter in KubOS.

Open the network configuration via ``nano overlay/etc/network/interfaces`` and change the ``iface eth0 inet static`` address assignment to match the network configuration of the test environment.
For example, if the IP Address of the RS3 should be ``192.168.1.75`` with a gateway at ``192.168.1.1`` use the following ::

    auto eth0
    iface eth0 inet static
        address 192.168.1.75
        netmask 255.255.255.0
        gateway 192.168.1.1

.. _build-kubos:

Build KubOS
***********

Once KubOS is configured, simply run the ``make`` command to start the build process.
The build process will take **an hour** for the initial build, with significant speed improvements in rebuilds.

.. note:: The ``make -j#`` where ``#`` is the number of cores allocated to the machine.

Once built, then KubOS can be burned onto a MicroSD card or turned into :doc:`an update file </pages/kubos/kubos-upgrade>`.

Once the ``make`` command finishes, the output can be copied from the ``buildroot-2019.02.2/output/images`` folder to an appropriate location.
Then the image can be copied onto a MicroSD card, please `follow the instructions here <https://docs.kubos.com/1.21.0/obc-docs/mbm2/installing-linux-mbm2.html#flash-the-sd-card>`_ for copying the image onto an SD card.
Or use the following to decompress the ``.tar.gz`` directly to a MicroSD card device ::

    $ cd buildroot-2019.02.2/output/images
    // Substitute X for block device for MicroSD card
    $ tar -xzOf kubos-linux.tar.gz | sudo dd of=/dev/sdX bs=1M status=progress

Once the `kubos-image.img` is copied onto a MicroSD card, follow the :doc:`RS3 Installation Documentation </pages/kubos/kubos-install>` for first-time installation of KubOS onto RS3.

.. _kubos-upgrade-creation:

Create & use update file
************************

The following assumes a previous KubOS image was successfully installed on the unit.
Turn on the RS3/FlatSat unit, and wait a bit for the MBM2 to boot.
Then connect to network via Ethernet cable and run the following commands ::

    $ scp kubos-linux-build/tools/kpack-<Name of upgrade file>.itb kubos@<upgrade target IP address>:/upgrade
    $ ssh kubos@<upgrade target IP address>
    ... enter password ...
    $ fw_setenv kubos_updatefile kpack-<Name of upgrade file>.itb
    $ reboot

Once rebooted, this will trigger the KubOS Upgrade process.

=============================
Mission Application Packaging
=============================

The following sections cover:

* Structure and layout of ``mission-apps`` package repo
* Packaging a mission application as part of buildroot
* Adding and using buildroot configuration options
* References to buildroot documentation

For more information on packaging, `see the buildroot manual <https://buildroot.org/downloads/manual/manual.html>`_ and reference the ``kubos`` and ``mission-apps`` examples.

In particular, see `Section 18.5.2 generic-package Reference <https://buildroot.org/downloads/manual/manual.html#_infrastructure_for_packages_with_specific_build_systems>`_ and ``LIBFOO_SITE_METHOD`` for directly integrating with a Git repository.

.. attention::
    The following sections cover how to package a mission application/service to be built as part of the buildroot process.
    
    Creating an image/upgrade file everytime a simple change is made to test development of an application is tedious and not recommended.

    Simply upload and test as part of the normal development process, and use the following to provide a standard mechanism of creating a base OS image or upgrade file to apply the changes/additions/removals to the satellite

    See `KubOS Mission Development Tutorial <https://docs.kubos.com/1.21.0/tutorials/first-mission-app.html>`_ and :ref:`mission-development` for more information.

.. note::
    KubOS is a customized buildroot configuration with additional services/features included to help facilitate creating a successful Flight-software package for a given mission.

    It is expected the user/mission management will fill in additional applications/hardware integrations/services to complete the necessary set of features needed for a particular mission.

.. figure:: /img/buildroot-process.png

    The depiction of the KubOS build process

Structure/Layout of mission-apps
********************************

We have now included a minimal `Mission Application <https://gitlab.com/pumpkin-space-systems/public/mission-apps>`_ as part of the Pumpkin fork of the KubOS repositories.
This is currently a bare-bones repository, with the aim of including more interactive examples in the future.

The application is broken down into:

* The ``mission-apps`` repository containing a bare-bones example applications in Python and Rust for a mission.
* A set of ``buildroot`` Makefiles inside of ``kubos-linux-build/packages/mission``

The ``mission-apps`` repo is the ``git`` repository that contains the mission application code to build as part of the image.

The ``kubos-linux-build`` scripts provide the necessary ``Makefiles`` in order to download/compile the necessary ``mission-app`` git repository and compile as part of the KubOS image build.

.. note::
    The ``mission.mk`` file inside of the ``kubos-linux-build/package/mission`` folder triggers the Bulidroot process to download and extract a Git/tar.gz archive into the ``buildroot-*/output/build`` folder.

    The rest of the ``Makefiles`` inside of the ``kubos-linux-build/package/mission`` directory reference the directory extracted inside of ``buildroot-*/output/build`` via the ``$(BUILD_DIR)/mission-$(MISSION_VERSION)`` environment variables to properly build each Rust/Python application.

.. note::
    The ``mission`` package ``Makefiles`` is simply a stripped-down version of the ``kubos`` package with less moving parts. Refer to the ``kubos`` package for a more advanced example as well.

Packaging an Application/Service
********************************

KubOS currently can integrate applications in Python and Rust easily and C/C++ with some additional work.

The following sections only cover **Python** and **Rust** applications -- C/C++ applications can see `The KubOS SDK for C/C++ here <https://docs.kubos.com/1.21.0/sdk-docs/sdk-c.html>`_.

Application Packaging
*********************

In order to have a package build as part of the ``buildroot`` make process, the user must:

#. Create a set of ``Makefiles`` and ``Config.in`` files to help build and describe configuration options for the package. *It is recommended to copy the existing example Python application and modify to suit needs.*
#. Edit the ``name-of-app.mk`` to pull from correct directory inside of ``$(BUILD_DIR)`` and set the correct environment variables.
#. Edit ``Config.in`` to include relevant configuration options to show up in buildroot configuration (see figure below for example)
#. Modify the ``init`` script to reference the correct application.
#. Reference variables created by environment variable name in ``name-of-app.mk`` Makefile to push configuration variables to a configuration fragment to be built for the ``mission-config.toml`` file.
#. Include the mission application as part of the ``kubos-linux-build/mission/Config.in`` to make ``buildroot`` aware of the package.
#. Test buildroot image make process, install on MBM2 to verify.

Python-specific notes
+++++++++++++++++++++

Python applications are installed via a ``setuptools`` type installation in a dedicated ``setup.py`` Python script.
Once *installed/copied into* on the ``buildroot`` build, the package is integrated as part of the ``rootfs`` that is included with the image generated.

Currently, the example mission application uses `console_scripts <https://python-packaging.readthedocs.io/en/latest/command-line-scripts.html>`_ to define a set of named applications that simply execute a python function when invoked.
For example, the ``pumpkin-mcu-service`` simply calls an ``execute`` function inside of ``pumpkin_mcu_service.service`` module to start the ``pumpkin-mcu-service`` to allow querying SupMCU modules, `see here for implementation <https://gitlab.com/pumpkin-space-systems/public/kubos/-/blob/master/services/pumpkin-mcu-service/setup.py#L45>`_.

Rust-specific notes
+++++++++++++++++++

Rust applications are built as part of the ``cargo build`` process. Note that ``buildroot`` currently doesn't support Rust natively like Python, however, is still fully able to package Rust application.

See `Integration of Cargo-based packages <https://buildroot.org/downloads/manual/manual.html#_integration_of_cargo_based_packages>`_ here for more information.

Makefiles creation
++++++++++++++++++

The creation of the ``Makefile`` is straightforward, simply copy the example folder and rename the ``python-mission-app.mk`` file to the ``name-of-containing-folder.mk``.

Note the following also shows the update needed to pull from the ``$(BUILD_DIR)`` for the particular example.

For a mission application called ``overseer-service``, this could be an example ``overseer-service.mk`` file adjusted from the example ::

    #####################################################
    #
    # Example Overseer Service installation
    #
    #####################################################
    OVERSEER_SERVICE_VERSION = 0.0.1
    OVERSEER_SERVICE_LICENSE = Apache-2.0
    OVERSEER_SERVICE_LICENSE_FILES = LICENSE
    OVERSEER_SERVICE_SITE = $(BUILD_DIR)/mission-$(MISSION_VERSION)/apps/overseer-service
    OVERSEER_SERVICE_SITE_METHOD = local
    OVERSEER_SERVICE_DEPENDENCIES = kubos
    OVERSEER_SERVICE_SETUP_TYPE = setuptools

    OVERSEER_SERVICE_INSTALL_STAGING = YES
    OVERSEER_SERVICE_POST_INSTALL_STAGING_HOOKS += PUMPKIN_MCU_INSTALL_STAGING_CMDS

    # Generate the config settings for the service and add them to a fragment file
    define PUMPKIN_MCU_INSTALL_STAGING_CMDS
        echo '[overseer-service.addr]' > $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo 'ip = ${BR2_OVERSEER_SERVICE_IP}' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo 'port = ${BR2_OVERSEER_SERVICE_PORT}' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo '' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo '[overseer-service]' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo 'string_var = ${BR2_OVERSEER_SERVICE_STRING_VAR}' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo '' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
    endef

    # Install the init script
    define OVERSEER_SERVICE_INSTALL_INIT_SYSV
        $(INSTALL) -D -m 0755 $(BR2_EXTERNAL_KUBOS_LINUX_PATH)/package/mission/overseer-service/overseer-service \
            $(TARGET_DIR)/etc/init.d/S$(BR2_OVERSEER_SERVICE_INIT_LVL)overseer-service
    endef

    $(eval $(python-package))

For more details, see `infrastructure for python packages <https://buildroot.org/downloads/manual/manual.html#_infrastructure_for_python_packages>`_ in offical Buildroot documentation.

Config.in
+++++++++

The ``Config.in`` file is a standard Buildroot configuration definition.

This allows the package to:

* Select required dependencies if a feature is toggled on/off
* Create custom configuration variables as part of the ``make menuconfig`` command
* Include help-tips/information about each option/package as part of ``make menuconfig`` dialog

An example, and commented ``Config.in`` package ::

    # Name of the environment variable base, the rest of environment variables in .mk file must 
    #   start with `OVERSEER_APP_*` to be properly picked up in Buildroot now.
    menuconfig BR2_PACKAGE_OVERSEER_APP 
    bool "Python Overseer app" 
    default n
    depends on BR2_PACKAGE_PYTHON3 # does NOT show if ``python3`` is not selected in buildroot
    select BR2_PACKAGE_PYTHON_GRAPHENE # automatically selects the ``graphine`` package
    select BR2_PACKAGE_KUBOS_SERVICE_LIB
    help
        Include the Example Python Overseer app # Help dialog for package
        
    if BR2_PACKAGE_OVERSEER_APP # Only show the following if the user has selected the ``overseer-app`` to be built as part of image

    # Configuration variables.

    config BR2_OVERSEER_APP_INIT_LVL
        int "Overseer app Init Run Level"
        default 20
        range 10 99
        depends on BR2_PACKAGE_OVERSEER_APP
        help
            The initialization priority level of the Python Overseer app.
            The lower the number, the earlier the service is initialized.

    config BR2_OVERSEER_APP_IP
        string "Overseer app IP Address"
        default "0.0.0.0"
        depends on BR2_PACKAGE_OVERSEER_APP
        help
            The IP address which the service should use to receive GraphQL requests

    config BR2_OVERSEER_APP_STRING_VAR
        string "String Var"
        default "/path/to/hello/world"
        depends on BR2_PACKAGE_OVERSEER_APP
        help
            An example string variable for the Overseer app
            
    config BR2_OVERSEER_APP_PORT
        int "Overseer app Port"
        default 8150
        depends on BR2_PACKAGE_OVERSEER_APP
        help
            The port which the service should use to receive GraphQL requests

    endif

For more information, see `Config.in documentation here <https://buildroot.org/downloads/manual/manual.html#writing-rules-config-in>`_

The ``init`` script
+++++++++++++++++++

The ``init`` script is the script that is installed to have the service start on boot.

The edit most of the time is pretty straightforward, simply change the name of the example application to the name of the application as defined in:

* The ``console_scripts`` variable in the ``setup.py`` variable for Python packages.
* The name of the generated executable for Rust packages.

For example, if the ``overseer-service`` was the name in the ``console_scripts`` variable in ``setup.py``, then the script would be ::

    #!/bin/sh

    # Start the Mission App in the background
    # passing in the location of the config.toml file 
    # (in the same directory as the service)
    # /usr/bin/overseer-service -c /etc/mission-config.toml &
    /usr/bin/overseer-service &

    exit 0

This is installed into the image using the corresponding ``runlevel`` as defined in the ``Config.in`` or buildroot-specific configuration.

Referencing ``Config.in`` variables
+++++++++++++++++++++++++++++++++++

When building the KubOS image, the various configuration options defined in the ``Config.in`` file for the package are referenced via environment variables.

These are directly used from the ``.mk`` file for the package to:

* Be put into a configuration file for the service.
* Alter the build process depending on value
* Enable/disable features depending on value.

The following shows how the Environment variables are referenced (note the ``BR2_OVERSEER_*`` environment variables injected by the ``Config.in``) ::

    # Generate the config settings for the service and add them to a fragment file
    define PUMPKIN_MCU_INSTALL_STAGING_CMDS
        echo '[overseer-service.addr]' > $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo 'ip = ${BR2_OVERSEER_SERVICE_IP}' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo 'port = ${BR2_OVERSEER_SERVICE_PORT}' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo '' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo '[overseer-service]' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo 'string_var = ${BR2_OVERSEER_SERVICE_STRING_VAR}' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
        echo '' >> $(MISSION_CONFIG_FRAGMENT_DIR)/overseer-service
    endef

Note at the end of the compilation process, the configuration files are appended into a single ``mission-config.toml`` file.

Once the ``Config.in`` file is changed appropriately, then edit the ``kubos-linux-build/package/mission/Config.in`` file to source the name of the application.
For an application called ``overseer-service`` the possible ``Config.in`` file could be ::

    # Create base `mission` option in config menu
    menuconfig BR2_PACKAGE_MISSION
        bool "Mission Packages"
        default y
        help
            Enable Mission services and libraries

    if BR2_PACKAGE_MISSION
        
        config BR2_MISSION_VERSION
            string "Mission Branch Source Version"
            default "master"
            help
                Release, tag, or branch of the Mission repo to use when building Mission
                packages

        
        source "$BR2_EXTERNAL_KUBOS_LINUX_PATH/package/mission/python-mission-app/Config.in"
        source "$BR2_EXTERNAL_KUBOS_LINUX_PATH/package/mission/rust-mission-app/Config.in"
        source "$BR2_EXTERNAL_KUBOS_LINUX_PATH/package/mission/overseer-service/Config.in"
    endif


.. _cross-compile-bbb:

Cross-Compile for BBB
+++++++++++++++++++++

In order to cross compile for the BBB, please ensure that:

1. The :ref:`Development Environment is setup <setup-process>`
2. KubOS image :ref:`has been configured <initial-build-setup>` and :ref:`then built <build-kubos>`

Once those have been met, the required GCC toolchain and compiled libraries are now available to build up the KubOS source.
Create an ``build.env`` file to store environment variables to be set before build.
Contents of the ``build.env`` ::

    # Edit to fill in path to the `buildroot` folder.
    BUILDROOT_DIR=/path/to/buildroot-2019.02.2

    # Export OPENSSL environment variables to fix ``cargo build`` for crate dependency
    export OPENSSL_LIB_DIR=$BUILDROOT_DIR/output/build/libopenssl-1.1.1a
    export OPENSSL_INCLUDE_DIR=$BUILDROOT_DIR/output/build/libopenssl-1.1.1a/include

    # Use the Buildroot toolchain's GCC linker
    export RUSTFLAGS="-Clinker=$BUILDROOT_DIR/output/images/bin/arm-linux-gcc"
    export TARGET_CC=$BUILDROOT_DIR/output/images/bin/arm-linux-gcc

    # Create an alias to strip off the debugging symbols from compiled rust apps
    alias arm-linux-strip=$BUILDROOT_DIR/output/images/bin/arm-linux-strip

Once ``source`` is run with the ``build.env``, navigate to the ``service`` and build for the specified BBB target ::

    $ source build.env

    # Build the File service for the BBB target
    $ cd /path/to/mission-apps/apps/service
    $ cargo build --target arm-unknown-linux-gnueabihf --release

    # Strip out symbols to save space
    $ arm-linux-strip target/arm-unknown-linux-gnueabihf/release/service
    $ scp target/arm-unknown-linux-gnueabihf/release/service kubos@<ip>:/home/kubos

Once copied onto the BBB, simply run the service via ``./service`` to start the compiled application.