.. _mission-development:

-------------------------------
Mission Development
-------------------------------

The following sections cover:

* Overview of developing applications/services
* Developing Python Applications
* Developing Rust Applications
* References to documentation

Mission Development
=======================

It is recommended to follow an iterative development process with:

#. Create buildroot config and image with any base Python packages missing 
    + Note: *Rust applications compile the libraries INTO the executable.*
#. Install image into development FlatSat/RS3/BBB
#. As needed, copy over the Python or Rust application/service and run locally to test.
#. Once the application is running as intended, see :ref:`app-packing` for how to integrate the application as part of KubOS build.

Application vs Service
++++++++++++++++++++++

KubOS mentions ``applications`` and ``services``. Typically ``applications`` are:

* Active executables that query/write/execute the mission
* Use ``services`` to interface with hardware/software/payload
* Are created as needed to provide functionality for the CONOPS of the mission

``Services`` on the other-hand are:

* Passive executables that receive queries/commands from ``applications``
* Interface directly with the hardware/payload/software, providing the necessary API's to interact with the system
* Are created as needed to provide interfaces/APIs to avionics of the S/C

.. note:: Some services may not be **completely passive**, as when they start up, they'll have to establish a link with their respective hardware/software/payload.

    In addition, there may be house-keeping to do between queries/mutations as well.

Developing Python Applications/Services
+++++++++++++++++++++++++++++++++++++++

Python applications/services are the harder of the two to setup (due to having to install dependencies and re-install the application everytime), but tend to be the easier to develop for.

Main considerations when developing a mission Python application/service:

* Are all of the relevant packages included on the current KubOS build?
    + Make sure all versions used for Python libraries match to what the development environment is using to avoid surprises.
    + Note KubOS currently has older versions of several Python packages, these would either have to be updated or the whole buildroot base needs to be updated.
* Considerations for Python Applications
    + If the application/service is heavily IO bound, then the difference between a Rust and Python implementation *should* be negligible, except for the slow *start-up performance* of Python.
    + Python is more familiar with developers than Rust, that may be a deciding factor if time is of the essence.
    + Python applications/services tend to be smaller than the Rust counterparts. However, if multiple Python libraries are required, this would negate this advantage.
    + Python applications/services will need to run the ``python setup.py install --force`` to apply an update on the BBB. There is a handy ``bbb-install.sh`` script that handles re-installation of a package automatically.
* What are the possible configuration options for this application/service?
    + Start by defining what the service should do, then figure out what static inputs are needed into the service to configure any possible parameters.
    + Don't make a service ``all-encompasing``, try to break out larger services/applications into several micro-services if needed. A good balance of too many/too little services depends on code style and mission design.

If the following above aren't considerations for using a Rust application instead, then it is recommended to:

#. Edit the ``setup.py`` to output a different package name, change authors, change ``console_scripts`` entry points, etc...
#. Edit the ``bbb-setup.sh`` to reference the correct package name in ``setup.py``. See below for example.
#. **Develop application code**
#. Run Application/Service on FlatSat/RS3 by copying over the files (via SCP `or other methods <https://docs.kubos.com/1.21.0/obc-docs/comms-setup.html#file-transfer>`_) and running ``./bbb-install`` to install the package.
#. Use the ``console_scripts`` entry_points to start the service.
#. Rinse and repeat for every Python application.


Developing Rust Applications/Services
+++++++++++++++++++++++++++++++++++++

Rust applications/services are the easier of the two *to setup* (due to the application compilation containing the libraries), but tend to be the **harder to develop** for due to the steeper learning curve of Rust.

Main considerations when developing a mission Rust application/service:

* Are there any compilation considerations for creates/libraries wanted that may be difficult to resolve for the BBB architecture?
    + For example, if a library requires ``SSE (SIMD)`` instructions to run, then that would fail because those ``SSE (SIMD)`` x86 instructions aren't on the ARM architecture.
* Considerations for Rust applications/services:
    + If the application is heavily CPU bound, then the runtime-performance difference between a Rust and Python implementation could be substantial, depending on the workload. Like C, Rust is a compiled language, and does NOT need an interpreter like Python.
    + Most KubOS applications/services are based in Rust rather than Python. There is more development examples with Rust than Python currently in the KubOS framework.
    + Applications are easier to install on the target than Python applications typically. Its a simple cross-compile for the target and copy the build output over, and run it.
    + KubOS provides a Rust example to integrate C libraries as part of a Rust package. If integration with C code is a necessity, this may be an easier route than integrating it with a Python application/service.
* What are the possible configuration options for this application?
    + Start by defining what the service should do, then figure out what static inputs are needed into the service to configure any possible parameters.
    + Don't make a service ``all-encompasing``, try to break out larger services/applications into several micro-services if needed. A good balance of too many/too little services depends on code style and mission design.

If the following above aren't considerations for using a Rust application/services instead, then it is recommended to:

#. Edit the ``cargo.toml`` to output a different application name, change authors, change dependencies, cargo features, etc...
#. **Develop application code**
#. Cross-compile Rust application for target, using ``--target arm-unknown-linux-gnueabihf`` in ``cargo build``, `see here for more details <https://docs.kubos.com/1.21.0/obc-docs/cross-compile.html#id1>`_.
#. Run Application on FlatSat/RS3 by copying over the files (via SCP `or other methods <https://docs.kubos.com/1.21.0/obc-docs/comms-setup.html#file-transfer>`_) and running the compiled application/service.
#. Rinse and repeat for every Rust application.
#. Package as part of ``buildroot`` to finish.

Documentation/Code References and Examples
++++++++++++++++++++++++++++++++++++++++++

Documentation Links:

* `Payload Services <https://docs.kubos.com/1.21.0/ecosystem/services/payload-services.html>`_
* `KubOS Service Configuration <https://docs.kubos.com/1.21.0/ecosystem/services/service-config.html>`_
* `KubOS Service Development <https://docs.kubos.com/1.21.0/ecosystem/services/service-dev.html>`_
* `KubOS Service Outline Guide <https://docs.kubos.com/1.21.0/ecosystem/services/service-outline-guide.html>`_
* `KubOS GraphQL Docs <https://docs.kubos.com/1.21.0/ecosystem/services/graphql.html>`_

    + `GraphQL Reference <https://graphql.org/learn/>`_
    + `Python GraphQL Library Reference <https://graphene-python.org/>`_
    + `Rust GraphQL Library Reference <https://graphql-rust.github.io/juniper/current/>`_

* `KubOS Application Development <https://docs.kubos.com/1.21.0/ecosystem/apps/app-guide.html>`_
* `Deployment application guide <https://docs.kubos.com/1.21.0/mission-dev/deployment.html>`_
* `KubOS Typical required applications <https://docs.kubos.com/1.21.0/mission-dev/mission-needs.html>`_
* `KubOS Scheduler docs <https://docs.kubos.com/1.21.0/ecosystem/services/scheduler.html#scheduler-service>`_
* `console_scripts packaging for Python <https://python-packaging.readthedocs.io/en/latest/command-line-scripts.html>`_
* `pumpkin-supmcu Python library <https://pumpkin-supmcu.readthedocs.io/en/latest/index.html>`_
* `Software Reference Manual <https://pumpkin-space-systems.gitlab.io/public/software-reference-manual/>`_

Software Links:

* `mission-apps example repository <https://gitlab.com/pumpkin-space-systems/public/mission-apps>`_
* `kubos-linux-build Pumpkin fork <https://gitlab.com/pumpkin-space-systems/public/kubos-linux-build>`_
* `pumpkin-supmcu implementation <https://gitlab.com/pumpkin-space-systems/public/pumpkin-supmcu/-/tree/master>`_
* KubOS-provided examples:

    + `Example apollo-fusion mission applications/services <https://github.com/kubos/apollo-fusion>`_
    + `Rust Service Example <https://gitlab.com/pumpkin-space-systems/public/kubos/-/tree/master/examples/rust-service>`_
    + `Rust Application Example <https://gitlab.com/pumpkin-space-systems/public/kubos/-/tree/master/examples/rust-mission-app>`_
    + `Rust C Example <https://gitlab.com/pumpkin-space-systems/public/kubos/-/tree/master/examples/rust-c-service>`_
    + `Python Application Example <https://gitlab.com/pumpkin-space-systems/public/kubos/-/tree/master/examples/python-mission-application>`_
    + `Python Service Example <https://gitlab.com/pumpkin-space-systems/public/kubos/-/tree/master/examples/python-service>`_

* Pumpkin-provided examples:

    + `pumpkin-mcu-service implementation <https://gitlab.com/pumpkin-space-systems/public/kubos/-/tree/master/services/pumpkin-mcu-service>`_
    + `python-mission-app <https://gitlab.com/pumpkin-space-systems/public/mission-apps/-/tree/master/apps/python-mission-app>`_
    + `rust-mission-app <https://gitlab.com/pumpkin-space-systems/public/mission-apps/-/tree/master/apps/rust-mission-app>`_ (Note: *This is a replica of the KubOS Rust Application*)