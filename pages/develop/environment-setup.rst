------------------------------------
KubOS Development Environment
------------------------------------

It is recommended, if heavy development is done with KubOS, to setup the dependencies for building and running KubOS applications natively.
Key points about building natively:

* No need to build/execute from inside of limited Vagrant VM
* Can develop with non-console based IDE/text editor easily
* Virtual IO performance increases the already *long* build times of the KubOS image *when building in VM*

The following section contain updates to the `base docs from KubOS <https://docs.kubos.com/1.21.0/getting-started/local-setup.html>`_

Notably, the `rustup` dependency does NOT need to be installed into the root user as the KubOS build scripts have been fixed to not require ``root`` permissions to build.

.. _setup-process:

Setup for Ubuntu 18.04
======================

KubOS uses a Ubuntu 18.04 Vagrant VM for the SDK. Setup is essentially the same as running the `Vagrant initialization scripts <https://github.com/kubos/kubos-vagrant/tree/master/kubos-dev/script>`_.

Install the dependencies for build ::

    $ sudo apt update && sudo apt install git python3 python3-pip curl gcc make pkg-config libssl-dev sqlite libsqlite3-dev libncurses-dev unzip
    $ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    $ rustup toolchain install 1.39.0-x86_64-unknown-linux-gnu
    $ rustup target add armv5te-unknown-linux-gnueabi --toolchain 1.39.0
    $ rustup target add arm-unknown-linux-gnueabihf --toolchain 1.39.0
    $ python3 -m pip install --user toml mock responses virtualenv
    $ cd ~/kubos-linux # create if doesn't exist
    $ virtualenv --system-site-packages -p /usr/bin/python3 env
    $ source env/bin/activate

Now follow the directions to build from :ref:`initial-build-setup`

Notes about Using SDK
=====================

.. attention:: Its not recommended to use the vagrant ``KubOS SDK`` anymore.

    It currently is out of date and needs

If the SDK environment **is preferred**, it is recommended to still develop on a Linux/Unix machine. This allows the user to symlink to the shared data folder (see :ref:`create-sdk-instance`) and still preserve the necessary file permissions needed for building any mission application/service.

To symlink, from within the Vagrant VM, execute ::

    $ ln -sF /path/to/shared/data/folder ~/kubos-linux/path/to/code

Once the Buildroot image is built, then push the relevant ``*.tar.gz`` files to the shared data folder.

Note the `warning here posted by KubOS <https://docs.kubos.com/1.21.0/deep-dive/klb/kubos-linux-on-mbm2.html#build-the-os-files>`_ about using the shared folder for the buildroot output.

.. warning:: If developing on a Windows machine, do NOT symlink to the data folder from within the VM and point the build output to the symlinked folder, due to the nature of NTFS on Linux, this can cause issues with building applications due to permissions.

    It is recommended to only push the ``*.tar.gz`` output to the shared data folder from within the VM, the buildroot process needs a Unix-based filesystem to properly build the images.