.. Pumpkin Sphinx Documentation documentation master file, created by
   sphinx-quickstart on Fri Nov  8 09:53:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==================================
Welcome to RS3/FlatSat User Manual
==================================

.. toctree::
   :maxdepth: 2
   :caption: Overview/Resources:

   pages/overview/overview
   pages/overview/resources

.. toctree::
   :maxdepth: 2
   :caption: RS3/FlatSat Resources:

   pages/interfaces/flatsat-interfaces
   pages/interfaces/rs3-interfaces
   pages/software/rs3-flatsat-software
   pages/software/rs3-flatsat-libraries
   pages/software/solar-simulator

.. toctree::
   :maxdepth: 2
   :caption: Flight Software:

   pages/kubos/kubos-upgrade
   pages/kubos/kubos-usage
   pages/kubos/kubos-install
   pages/kubos/kubos-sdk
   pages/develop/environment-setup
   pages/develop/mission-app
   pages/develop/buildroot

====================
RS3 Contents
====================

The Rack-mount Simulator 3 (RS3) is meant to host a variety of SUPERNOVA Bus modules as well as a number of QSM modules, a simulator module made to emulate up-to 4 SUPERNOVA bus modules.
A typical RS3 unit will come loaded with the following CubeSat Kit bus modules:

* Motherboard Module 2 (MBM2)
* 2x Quad-SupMCU (QSM) modules
* Quad RS-232 module

.. note:: Additional modules can be purchased for usage in the RS3, contact Pumpkin for more information.

In addition, the RS3 typically has the following interfaces on the outside:

* 4x USB-B Ports connected to internal SUPERNOVA Bus modules
* 4x 10/100 Ethernet Ports
* 4x RS-232 DB9 Ports connected to CSK UART's
* 28V PSU Output

See RS3 Datasheet for more information on electrical connections and physical characteristics.

================
FlatSat Contents
================

The FlatSat is meant to host a variety of SUPERNOVA EM Bus modules. The typical FlatSat will usually come with:

* Motherboard Module 2 (MBM2)
* Desktop CubeSat Power Supply (DCPS)
* Payload Interface Module (PIM) [optional]
* GPS Receiver Module (GPSRM) [optional]
* Radio Host Module 1/2 (RHM1/2) [optional]

There are other modules that can be integrated into the FlatSat as well.

See specific FlatSat configuration and datasheets for modules on the FlatSat for more information.

====================
RS3/FlatSat Software
====================

RS3/FlatSat hosts the Pumpkin Telemetry Discovery Injection and Generation (PuTDIG) suite of CLI programs, with a web-based UI version as well.
The PuTDIG CLI suite consists of three separate applications:

* **pumqry** - Discovery and telemetry capture of SUPERNOVA Bus modules or QSM modules
* **pumgen** - Generation of new telemetry to be injected into QSM modules inside of RS3 unit
* **puminj** - Injection of generated telemetry sets into QSM modules inside of RS3 unit

In addition, there is a web-based version of the PuTDIG program meant to:

* Web-based UI interface, standalone server
* Easily visualize telemetry on each SupMCU Module
* Run directly from KubOS image hosted on MBM2 in RS3
* Option to run independent of MBM2 inside of RS3 unit, via external host connected by 2x5 Box Connector on front of RS3
* Handles discovery, generation/editing and injection of telemetry into QSM modules

There is two Python 3.7+ libraries included in the KubOS image intended as a programmatic interface into the firmware of the SupMCU modules:

* `pumpkin-supmcu <https://pumpkin-supmcu.readthedocs.io/en/latest/index.html>`_ - A pure-python library for handling the business logic of I2C transactions with SupMCU modules
* ``pumpkin-supmcu-kubos`` - Provides ``I2CMaster`` for Kubos via ``pumpkin_supmcu.kubos.KubosI2CMaster``

For FSW interface with FSW applications/services in KubOS, please use the ``pumpkin-mcu-service`` contained under `the public fork of KubOS from Pumpkin <https://gitlab.com/pumpkin-space-systems/public/kubos/-/tree/master/services/pumpkin-mcu-service>`_.

Finally, for a full telemetry and command reference for the SUPERNOVA Bus modules, please see the `Software Reference Manual <https://pumpkin-space-systems.gitlab.io/public/software-reference-manual/>`_.

.. note:: This is a living document to receive updates over-time as features are refined and added to the RS3/FlatSat. Keep checking for the latest updates and information.

==================
Indices and tables
==================

.. csv-table:: Revisions
    :header: "Author", "Version", "Description", "Date"
    :widths: 10, 10, 60, 10

    "JRW/AS", "v1.0", "Initial Version", "2020-12-10"
    "JRW", "v1.1", "Added develop section, updated environment setup, updated KubOS build to not include sudo", "2021-02-03"
    "JRW/AS", "v1.2", "Fixed typos, added more links for documentation, cleaned up mission-development section, added more details to mission-development", "2021-02-04"
    "JRW", "v1.3", "Refactored sections more logically into different major sections. Added additional information and resources ", "2021-13-05"
    "JRW", "v1.4", "Added sections for environment setup and cross-compiling rust programs for BBB."

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
