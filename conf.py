# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import datetime
import os
import sys
import sphinx_rtd_theme

#sys.path.insert(0, os.path.abspath('../'))

# Insert the ext folder inside of the project to have the ability to load custom extensions.
sys.path.insert(0, os.path.abspath('./ext'))

# -- Pull version from git tag -----------------------------------------------

# When GitLab is building the docs, there are OS Environment variables that
# get injected into the build process that signify why the build was started. 
# We check for these variables in this order:
#   1. Build was started from a git tag (see `Lightweight Tags` here: 
#      https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag).
#      This allows the production GitLab pages site to be versioned.
#   2. Build was started from a commit. This allows us to deploy a test version
#      of the site if needed, and have a build number associated with it.
# If neither are found in the OS Environment variables, then assume `custom`
# to not have the build choke when testing locally.

if os.environ.get('CI_COMMIT_TAG'):
    # release = os.environ['CI_COMMIT_TAG']
    release = 'v1.2.0'  # JRW: Use fixed version string here ... theres a better solution for this ...
elif os.environ.get('CI_JOB_ID'):
    # release = os.environ['CI_JOB_ID']
    release = 'v1.2.0'  # JRW: Use fixed version string here ... theres a better solution for this ...
else:
    release = 'local'  # Makes sure the read the docs version can build.



# -- Project information -----------------------------------------------------

project = 'Pumpkin RS3/FlatSat User Guide'
copyright = '{}, Pumpkin Inc'.format(datetime.datetime.now().year)
author = 'Ashton Meginnis, James Womack, Austin Small'

# Grab the date and time the documentation was built and set to |today| substitution.
today = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx_autodoc_typehints',
    'sphinx.ext.intersphinx',
    'sphinx_rtd_theme'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# Dictionary that declares a sphinx cross-reference source. More Examples to be
# made, see https://gitlab.com/pumpkin-space-systems/public/pumpkin-supmcu/blob/master/docs/conf.py
# for an example of how to connect the python docs...
intersphinx_mapping = {
    #'sphinx': ('http://www.sphinx-doc.org/en/master', None) # for reference
}


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
html4_writer = True

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# def setup(app):
#     app.add_css_file('custom.css')
